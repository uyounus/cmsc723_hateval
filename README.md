### Hats-off: A Multilingual Hate Speech Detector for English and Spanish Tweets

1. Preprocessing

To run the most current preprocessing script over all raw tweets:
- Ensure you have the data files organized locally as follows (zipped raw files are on [google drive](https://drive.google.com/drive/u/0/folders/19HEuWLi5o7kiiYfWAxWrnOmWVkgl1KWf])):

````
├── raw
│   ├── dev
│   │   ├── dev_en.tsv
│   │   ├── dev_es.tsv
│   │   ├── train_en.tsv
│   │   └── train_es.tsv
│   └── reference_test
│       ├── en.tsv
│       └── es.tsv
├── sample_raw
│   ├── dev
│   │   ├── dev_en_sample.tsv
│   │   ├── dev_es_sample.tsv
│   │   ├── train_en_sample.tsv
│   │   └── train_es_sample.tsv
│   └── reference_test
│       ├── en_sample.tsv
│       └── es_sample.tsv
````

- Navgiate to the project root directory, and create conda env:

  `conda env create -f cmsc723.yml`
  
- For packages that cannot be installed by anaconda, like spellchecker, please use

  `pip install pyspellchecker`
 
- Finally, activate the environment 
  
  `source activate cmsc723`
  
- If you want to generate 1% samples of the full datasets, run (arg 2 should be the path to the data dir):
 
  `python3 ./src/preprocessing/generate_data_samples.py "./data"`

- Make changes to the example config file if necessary (e.g., to point to the correct subdir locations).

- Download Spacy models:

  - `python -m spacy download en_core_web_sm`
  - `python -m spacy download es_core_web_sm`
  - `python -m spacy download en`


- Run script to preprocess each raw file, and write the result to the associated preprocessed directory:
  - `cd ./src/preprocessing`
  - `sh preprocess_raw_files.sh`

### Note: 
For more details, please find:

- Project Poster [here](./CMSC723_HateEval_Poster.pdf)!

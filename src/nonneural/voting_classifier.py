import numpy as np
import pandas as pd
from os.path import join
from utils import get_label, list2num, all_metrics
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.linear_model import LogisticRegression
from sklearn.naive_bayes import GaussianNB
from sklearn.ensemble import RandomForestClassifier, VotingClassifier
import timeit

import sys
# sys.path.insert(0, '../../')
sys.path.insert(0, '../../src')

from augmenting_features import gen_aug_features
from augmenting_features.gen_aug_features import gen_count_and_bool_features

def init_clf():
    clf1 = LogisticRegression(random_state=1)
    clf2 = RandomForestClassifier(n_estimators=50, random_state=1)
    clf3 = GaussianNB()
    all_clf = VotingClassifier(estimators=[('lr', clf1), ('rf', clf2), ('gnb', clf3)], voting='hard')
    return all_clf

def run_embed_lr(embed_dict,embed_dim, train_dat_df, test_dat_df, train_lb_df, test_lb_df, task_b,augment):
    """
    main function
    """
    start_time = timeit.default_timer()
    train_lb, _ = get_label(train_lb_df, task_b)
    test_lb, _ = get_label(test_lb_df, task_b)
    #print(train_lb)
    #print(test_lb)
    #train_lb = train_lb
    #test_lb = test_lb
    #train_dat_df = train_dat_df
    #test_dat_df = test_dat_df
    #print('Before, train_daf_df',train_dat_df[:3])
    train_embed = dat2embed(train_dat_df,embed_dim)
    test_embed = dat2embed(test_dat_df,embed_dim)

    if len(augment.keys()) > 0:
        for i,aug_feature_set in enumerate(augment["train"]):

            train_aug_list = augment["train"][i]
            test_aug_list = augment["test"][i]
            
            # extend is an in-place operation 
            [x.extend(train_aug_list[idx]) for idx,x in enumerate(train_embed)]
            [x.extend(test_aug_list[idx]) for idx,x in enumerate(test_embed)]



    #for i in range(len(train_embed)-1):
        #if (len(train_embed[i]) != len(train_embed[i+1])):
        #    print("The length of No.%d embedding in train_embed = %d"%(i, len(train_embed[i])))
    #for j in range(len(test_embed)-1):
        #if (len(test_embed[j]) != len(test_embed[j+1])):
        #    print("The length of No.%d embedding in test_embed = %d"%(j, len(test_embed[j])))
    
    #TODO: more classifiers
    if task_b:
        all_clf = init_clf()
        label_1 = [int(items[0]) for items in train_lb]
        all_clf.fit(train_embed, label_1)
        pred_label_1 = all_clf.predict(test_embed)

        all_clf = init_clf()
        label_2 = [int(items[1]) for items in train_lb]
        all_clf.fit(train_embed, label_2)
        pred_label_2 = all_clf.predict(test_embed)
        
        all_clf = init_clf()
        label_3 = [int(items[2]) for items in train_lb]
        all_clf.fit(train_embed, label_3)
        pred_label_3 = all_clf.predict(test_embed)
        pred_lb = []
        # pred lb is [[y11,y12,y13], [y11,y22, y23], ...]
        for i in range(len(pred_label_1)):
            tmp = [pred_label_1[i], pred_label_2[i], pred_label_3[i]]
            pred_lb.append(tmp) 
        result = all_metrics(pred_lb, test_lb, task_b)
    else:
        train_lb = [int(item) for item in train_lb]
        test_lb = [int(item) for item in test_lb]
        all_clf = init_clf()
        all_clf.fit(train_embed, train_lb)
        pred_lb = all_clf.predict(test_embed)
        result = all_metrics(pred_lb, test_lb, task_b)
    
    print(result)
    print("During Embed training, %0.3f seconds elapsed."%(timeit.default_timer() - start_time))

def dat2embed(data_df,embed_dim):
    """
    input: a list of strings
    output: the averaged embedding
    """
    all_embed = []
    all_w = 0
    ovw = 0
    for sentence in data_df:
        token_ls = sentence.split(' ')
        stn_embed = [0]*embed_dim
        for tokens in token_ls:
            all_w += 1
            try:
                word_embed = embed_dict[tokens]
                if len(word_embed) == 0:
                    ovw += 1
                    with open('avg_embed.txt') as in_f:
                        word_embed = [float(item) for item in in_f.readline().strip('\n').split(' ')][:embed_dim]
            except:
                #print(tokens)
                ovw += 1
                # for out-of-vocabulary words, see https://stackoverflow.com/questions/49239941/what-is-unk-in-the-pretrained-glove-vector-files-e-g-glove-6b-50d-txt
                with open('avg_embed.txt') as in_f:
                    word_embed = [float(item) for item in in_f.readline().strip('\n').split(' ')][:embed_dim]
            stn_embed = [sum(x) for x in zip(stn_embed, word_embed)]
        # use the average
        stn_embed = [x/len(token_ls) for x in stn_embed]
        all_embed.append(stn_embed)
    print("all_embed first 3",len(all_embed),len(all_embed[0]))
    print("%0.2f words are out-of-vocabulary"%(1.0*ovw/all_w))

    return all_embed

def load_embed(embed_file):
    """
    input: the embedding file
    output: a dictionary, key: word, value: a list of embeddings
    """
    with open(embed_file) as embed_f:
        tmp = [x.strip().split(' ') for x in embed_f.read().splitlines()][1:]
    embed_dict = {}
    for i in range(len(tmp)):
        float_embed = [float(item) for item in tmp[i][1:]]
        embed_dict[tmp[i][0]] = float_embed
    return embed_dict

if __name__ == "__main__":
    # parameters
    language = "en"
    maximum_features = None
    task_b = False
    task_b = True
    if language == "en":
        print("In English")
    else:
        print("In Spanish")
    fdir = "../../data/preprocessed/"
    edge_list_dir = "../../data/edge_lists"
    embed_dim = 200
    embed_file = "../../data/glove.twitter.27B/glove.twitter.27B.200d.txt"
    #embed_file = "/fs/cbcb-lab/mdml/users/trachen/glove.twitter.27B.200d.txt"
    #embed_file = "/Users/yuexichen/Downloads/wiki.es/wiki.es.vec"
    # embed_file ="/fs/cbcb-lab/mdml/users/trachen/es/es_embed.txt"
    train_data_f = join(fdir, "dev/preproc_train_%s.tsv"%language)
    test_data_f = join(fdir, "reference_test/preproc_%s.tsv"%language)
    train_lb_df = pd.read_csv(train_data_f, sep='\t').iloc[:,2:5]
    test_lb_df = pd.read_csv(test_data_f, sep='\t').iloc[:,2:5]
    train_dat_df = pd.read_csv(train_data_f, sep='\t').iloc[:,1].values.tolist()
    test_dat_df = pd.read_csv(test_data_f, sep='\t').iloc[:,1].values.tolist()
    test_lb_df.to_csv('en_test_label.csv', sep='\t')

    aug = ["counts_and_bools"]
    features = ["hashtags", "mentions", "expletives", "racial_slurs", "emojis", "names"]


    train_dat_aug_f = gen_count_and_bool_features(fdir, edge_list_dir, language, "dev", "train", 
                                    "preproc_train_{}.tsv".format(language), features)
    
    train_cb_aug_df = train_dat_aug_f.iloc[:,1:].values.tolist()
    
    test_dat_aug_f = gen_count_and_bool_features(fdir, edge_list_dir, language, "reference_test", "reference_test", 
                                    "preproc_{}.tsv".format(language), features)
    
    test_cb_aug_df = test_dat_aug_f.iloc[:,1:].values.tolist()


    embed_dict = load_embed(embed_file)
    run_embed_lr(embed_dict,embed_dim, train_dat_df, test_dat_df, train_lb_df, test_lb_df, task_b,
                     {"train":[train_cb_aug_df], "test":[test_cb_aug_df]})
    

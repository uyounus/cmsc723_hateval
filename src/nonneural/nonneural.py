import numpy as np
import pandas as pd
from os.path import join
from src.nonneural.utils import get_label, list2num, all_metrics
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.linear_model import LogisticRegression
import timeit
# #%run ../augmenting_features/gen_aug_features.py
# from augmenting_features import gen_aug_features

def run_ngram_lr(train_dat_df, test_dat_df, train_lb_df, test_lb_df, task_b, maximum_features, augment:dict):
    """
    LR with n-grams
    input: train_data_df, test_data_df, train_label, test_label
    output: all metrics
    """
    start_time = timeit.default_timer()
    vectorizer = TfidfVectorizer(ngram_range=(1,3), max_features=maximum_features)
    train_lb, _ = get_label(train_lb_df, task_b)
    test_lb, _ = get_label(test_lb_df, task_b)
    train_vec = vectorizer.fit_transform(train_dat_df)
    feature_names = vectorizer.get_feature_names()
    #ratio can be adjusted smaller for test
    ratio = len(feature_names)
    print('len = ',ratio)
    train_mat = train_vec.todense().tolist()[:ratio]
    train_lb = train_lb[:ratio]
    test_vec = vectorizer.transform(test_dat_df)
    test_mat = test_vec.todense().tolist()[:ratio]
    test_lb = test_lb[:ratio]
    print("For TFIDF, %0.3f seconds elapsed."%(timeit.default_timer() - start_time))
    

    if len(augment.keys()) > 0:
        
        for i,aug_feature_set in enumerate(augment["train"]):

            train_aug_list = augment["train"][i]
            test_aug_list = augment["test"][i]
            
            # extend is an in-place operation 
            [x.extend(train_aug_list[idx]) for idx,x in enumerate(train_mat)]
            [x.extend(test_aug_list[idx]) for idx,x in enumerate(test_mat)]

#             for idx, _ in enumerate(train_mat):

#                 train_mat[idx].extend(train_aug_list[idx])
            
#             for idx, _ in enumerate(test_mat):

#                 test_mat[idx].extend(test_aug_list[idx])
                
                
            
            
#             #print(train_mat[0:5])
#             train_mat = [[x.extend(train_aug_list[j])] for j,x in enumerate(train_mat)]
#             #print(test_aug_list[0:5])
#             #print(train_mat[0:5])
#             test_mat = [[x.extend(test_aug_list[j])] for j,x in enumerate(test_mat)]
            
#             print(train_mat[0:5])

    #if "counts_and_bools" in augment:

        
    #if "knowledge_graph" in augment:

    if task_b:
        start_time = timeit.default_timer()
        clf1 = LogisticRegression(solver = 'lbfgs', max_iter=500)
        label_1 = [items[0] for items in train_lb]
        clf1.fit(train_mat, label_1)
        pred_label_1 = clf1.predict(test_mat)

        clf2 = LogisticRegression(solver = 'lbfgs', max_iter=500)
        label_2 = [items[1] for items in train_lb]
        clf2.fit(train_mat, label_2)
        pred_label_2 = clf2.predict(test_mat)
        
        clf3 = LogisticRegression(solver = 'lbfgs', max_iter=500)
        label_3 = [items[2] for items in train_lb]
        clf3.fit(train_mat, label_3)
        pred_label_3 = clf3.predict(test_mat)
        pred_lb = []
        # pred lb is [[y11,y12,y13], [y11,y22, y23], ...]
        for i in range(len(pred_label_1)):
            tmp = [pred_label_1[i], pred_label_2[i], pred_label_3[i]]
            pred_lb.append(tmp) 
        print("For TFIDF LG")
        res = all_metrics(pred_lb, test_lb, task_b)
        print("During LG training, %0.3f seconds elapsed."%(timeit.default_timer() - start_time))
        return res
    else:
        start_time = timeit.default_timer()
        clf = LogisticRegression(solver = 'lbfgs', max_iter=500)
        clf.fit(train_mat, train_lb)
        pred_lb = clf.predict(test_mat)
        print("For TFIDF LG")
        res = all_metrics(pred_lb, test_lb, task_b)
        print("During LG training, %0.3f seconds elapsed."%(timeit.default_timer() - start_time))
        return res



# +
if __name__ == "__main__":
    # parameters
    language = "en"
    maximum_features = 30000
    task_b = False
    if language == "en":
        print("In English")
    else:
        print("In Spanish")
    fdir = "../../data/preprocessed/"
    edge_list_dir = "../../data/edge_lists"
    train_data_f = join(fdir, "dev/preproc_train_%s.tsv"%language)
    test_data_f = join(fdir, "reference_test/preproc_%s.tsv"%language)
    train_lb_df = pd.read_csv(train_data_f, sep='\t', header=None).iloc[:,2:5]
    test_lb_df = pd.read_csv(test_data_f, sep='\t', header=None).iloc[:,2:5]
    train_dat_df = pd.read_csv(train_data_f, sep='\t', header=None).iloc[:,1].values.tolist()
    test_dat_df = pd.read_csv(test_data_f, sep='\t', header=None).iloc[:,1].values.tolist()
    #15k in vocabulary
    
    aug = ["counts_and_bools"]
    features = ["hashtags", "mentions", "expletives", "racial_slurs", "emojis", "names"]


    train_dat_aug_f = gen_count_and_bool_features(fdir, edge_list_dir, language, "dev", "train", 
                                    "preproc_train_{}.tsv".format(language), features)
    
    train_cb_aug_df = train_dat_aug_f.iloc[:,1:].values.tolist()
    
    test_dat_aug_f = gen_count_and_bool_features(fdir, edge_list_dir, language, "reference_test", "reference_test", 
                                    "preproc_{}.tsv".format(language), features)
    
    test_cb_aug_df = test_dat_aug_f.iloc[:,1:].values.tolist()

    # call functions
    run_ngram_lr(train_dat_df, test_dat_df, train_lb_df, test_lb_df, task_b, maximum_features,
                 {"train":[train_cb_aug_df], "test":[test_cb_aug_df]})
    
#     run_ngram_lr(train_dat_df, test_dat_df, train_lb_df, test_lb_df, task_b, maximum_features,
#                  {"train":[train_cb_aug_df], "test":[test_cb_aug_df]})
# -





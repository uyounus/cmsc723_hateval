import pandas as pd
import numpy as np
from os.path import join
from collections import Counter
from sklearn.metrics import accuracy_score, precision_score, recall_score, f1_score, confusion_matrix

def get_label(data_pd, task_b=False):
    """
    input: csv file
    output: labels associated with each twitter
    note: label list: as a list of tuples, and the most frequent label
    """
    if task_b:
        all_label = [tuple(item) for item in data_pd.values.tolist()]
        mf_result = Counter(all_label).most_common(1)
        mf_label = mf_result[0][0]
        mf_cn = mf_result[0][1]
        print("There are %d labels in the dataset, and the most frequent label is (%d, %d, %d), which occured %d times"%(len(all_label), mf_label[0], mf_label[1], mf_label[2], mf_cn))
    else:
        #only take the hate speech label
        all_label = [item[0] for item in data_pd.values.tolist()]
        mf_result = Counter(all_label).most_common(1)
        mf_label = mf_result[0][0]
        mf_cn = mf_result[0][1]
        print("There are %d labels in the dataset, and the most frequent label is %d, which occured %d times"%(len(all_label), mf_label, mf_cn))
    return all_label, mf_label

def list2num(pred_ls, real_ls, task_b=False):
    """
    This function only used in computing exact match ratio (EMR) in task B
    input: predicted list, groundtruth list
    output: multiclass indicators
    e.g. for class label [1,0,1], the multiclass indicator is `101`
    if a list of labels is [[1,0,1],[0,0,1]], then the returned list is ['101', '001']
    """
    for i in range(len(pred_ls)):
        #pred_ls is of the same length of real_ls
        pred_y = [''.join([str(int(i0)) for i0 in item0]) for item0 in pred_ls]
        real_y = [''.join([str(int(i1)) for i1 in item1]) for item1 in real_ls]
    return pred_y, real_y

def all_metrics(pred_ls, real_ls, task_b=False):
    """
    input: pred_ls, real_ls
    output: 
    Task A: accuracy, precision, recall, macro F1_score,
    Task B: averaged macro F1_score across 3 types, Exact Match Ratio (EMR)

    # for ref: sklearn api format: sklearn.metrics.accuracy_score(y_true, y_pred, normalize=True, sample_weight=None)
    """
    if task_b:
        all_f1 = 0
        all_emr = 0
        for i in range(3):
            each_pred = [item0[i] for item0 in pred_ls]
            each_real = [item1[i] for item1 in real_ls]
            each_f1 = f1_score(each_real, each_pred, average='macro')
            all_f1 += each_f1
        avg_f1 = all_f1/3
        # the difference between avg_f1 and EMR is that Exact Match requires all 3 labels are correct for a given twitter
        # turn 3 labels into a single string
        pred_y, real_y = list2num(real_ls, pred_ls)
        emr_count = accuracy_score(real_y, pred_y, normalize=False)
        emr_score = emr_count / len(pred_ls)
        cm = confusion_matrix(real_y, pred_y, labels=['000', '001', '010', '011', '100', '101', '110', '111'])
        print("For Task B, \n average macro F1 score: %0.3f, \n Exact Match Ratio score: %0.3f"%(avg_f1, emr_score))
        return {"macro_avg_f1": avg_f1, "emr": emr_score, "confusion_matrix":cm}
    else:
        acc = accuracy_score(real_ls, pred_ls)
        pr = precision_score(real_ls, pred_ls)
        rs = recall_score(real_ls, pred_ls)
        f1 = f1_score(real_ls, pred_ls, average='macro')
        cm = confusion_matrix(real_ls, pred_ls)
        print("For Task A, \n Accuracy: %0.3f, \n Precision: %0.3f, \n Recall: %0.3f, \n Macro F1 score: %0.3f"%(acc, pr, rs, f1))
        return {"acc":acc, "pr": pr, "rs": rs, "macro_avg_f1":f1, "confusion_matrix":cm}






#!/bin/bash
RAW_DATA_DIR=../../data/raw
cd $RAW_DATA_DIR

for i in $RAW_DATA_DIR/*/*.tsv; do
  echo "Processing $i"
	source activate cmsc723;
	if [[ $i =~ "es" ]]
  then
	  python ../../src/preprocessing/preprocess.py ../../src/spanish_config.ini $i
	else
	  python ../../src/preprocessing/preprocess.py ../../src/english_config.ini $i
	fi
done

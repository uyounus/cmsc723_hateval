#!/usr/bin/env python3
import pandas as pd
import numpy as np
import os
from collections import OrderedDict, Counter
import re
import textacy
from textacy import preprocessing
import spacy
import configparser
import random
import emoji
from spellchecker import SpellChecker
import sys
import platform
import pickle

# EXTRACTION METHODS # 
def extract_mentions(row, res, *args):
    
    mentions = re.findall(r'([@]\w+)', row["text"])
    res[row["id"]] = {i:mention for i, mention in enumerate(mentions)}
    
    return

def extract_hashtags(row, res, *args):
    
    hashtags = re.findall(r'([#]\w+)', row["text"])
    res[row["id"]] = {i:hashtag for i, hashtag in enumerate(hashtags)}
    
    return

def extract_urls(row, res, *args): 
    
    urls = re.findall(r'http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\), ]|(?:%[0-9a-fA-F][0-9a-fA-F]))+', row["text"])
    res[row["id"]] = {i:url for i,url in enumerate(urls)}
    
    return

def extract_expletives(row, res, **kwargs):
    
    # need a language-specific lookup list; len of tweet << len of expletives, so just tokenize and return all matches
    # TODO: consider misspellings, and/or fuzzy matches, phonetic spelling, or stuff like a$$, etc.

    # english expletives source is currently: https://www.cs.cmu.edu/~biglou/resources/bad-words.txt

    expletives = [x.lower() for x in row["text"].split() if x in [y for y in kwargs["words"][0]]]
    res[row["id"]] = {i:expletive for i, expletive in enumerate(expletives)}
    
    return 

def extract_racial_slurs(row, res, **kwargs):

    # current source for English terms is : https://en.wiktionary.org/w/index.php?title=Category:English_ethnic_slurs
    slurs = [x.lower() for x in row["text"].split() if x in [y for y in kwargs["words"][0]]]
    res[row["id"]] = {i:slur for i,slur in enumerate(slurs)}
    
    return

def extract_emojis(row, res, *args):
    
    # row 4467 has an emoji    
    e_counter = Counter()
    
    text = emoji.demojize(row["text"])
    text = re.findall(r'(:[^:]*:)', text)
    
    for x in text:
        if emoji.emojize(x) in emoji.UNICODE_EMOJI:
            
            # we can change the key to be the text representation if that's easier to look up embeddings for
            e_counter[emoji.emojize(x)] += 1

    res[row["id"]] = e_counter
    
    return 


def extract_names(row, res, spacy_lang, *args):
        
    names = textacy.extract.entities(textacy.make_spacy_doc(row["text"], spacy_lang))
    res[row["id"]] = {i:name.text for i, name in enumerate(names)}
    
    return 

def extract_all(config, df, func_dict_pairs, lang, word_lists):
    
    for f, d in func_dict_pairs:
        
        if config["preprocessing.extract"][f.__name__]:
        
            print("APPLYING {}.".format(f.__name__))

            if f.__name__ == "extract_expletives":
                df.apply(f, res=d, words = word_lists[lang]["expletives"], axis=1)

            elif f.__name__ == "extract_racial_slurs":
                 df.apply(f, res=d, words = word_lists[lang]["slurs"], axis=1)
            
            elif f.__name__ == "extract_names":
                df.apply(f, res=d, spacy_lang = config["paths"]["SPACY_LANG"], axis=1)

            else:
                df.apply(f, res=d,  axis=1)
        
    print("DONE")
    return

def extraction_main(config, df, filename):
    
    mentions_dict = OrderedDict()
    hashtags_dict = OrderedDict()
    urls_dict = OrderedDict()
    expletives_dict = OrderedDict()
    slurs_dict = OrderedDict()
    emojis_dict = OrderedDict()
    names_dict = OrderedDict()
    

    funcs = [extract_mentions, extract_hashtags, extract_urls, extract_expletives, extract_racial_slurs, extract_emojis, extract_names]
    dicts = [mentions_dict, hashtags_dict, urls_dict, expletives_dict, slurs_dict, emojis_dict, names_dict]
    func_dicts = zip(funcs, dicts)

    word_lists = OrderedDict()

    word_lists[config["paths"]["LANG"]] = {}

    if config["paths"]["LANG"] == "es":

        word_lists[config["paths"]["LANG"]]["expletives"] = pd.read_csv(config["paths"]["EXPLETIVES"], encoding="utf-16", header=None)
        word_lists[config["paths"]["LANG"]]["slurs"] = pd.read_csv(config["paths"]["SLURS"], encoding="utf-8", header=None)

    else:
        word_lists[config["paths"]["LANG"]]["expletives"] = pd.read_csv(config["paths"]["EXPLETIVES"], encoding="utf-8", header=None)
        word_lists[config["paths"]["LANG"]]["slurs"] = pd.read_csv(config["paths"]["SLURS"], encoding="utf-8", header=None)


    #word_lists["es"] = {}
    #word_lists["es"]["expletives"] = pd.read_csv(config["paths"]["EXPLETIVES"], encoding="utf-8", header=None)
    #word_lists["es"]["slurs"] = pd.read_csv(config["paths"]["SLURS"], encoding="utf-8", header=None)

    extract_all(config, df, func_dicts, config["paths"]["LANG"], word_lists)

    edge_list_names = ["mentions", "hashtags", "urls", "expletives", "racial_slurs", "emojis", "names"]
    for i, d in enumerate(dicts):
        pickle_filename = "{}_{}".format(os.path.join(config["paths"]["EDGE_LIST_PATH"], edge_list_names[i]), filename.split(".tsv")[0])
        print(pickle_filename)
        
        with open('{}.pickle'.format(pickle_filename), 'wb') as handle:
            pickle.dump(d, handle, protocol=pickle.HIGHEST_PROTOCOL)

    return mentions_dict, hashtags_dict, urls_dict, expletives_dict, slurs_dict, emojis_dict, names_dict

def fix_typos(tweet):
    
    # I don't think this will work very well for twitter data. char-level ngrams definitely a better way to go
    # but, it's here as a foundation in case anyone wants to modify or extend it
    
    spell = SpellChecker()
    tokens = tweet.split()
    misspelled = spell.unknown(tokens)

    for i, word in enumerate(tokens):
        if word in misspelled:
            tokens[i] = spell.correction(word)
    
    return ' '.join(t for t in tokens)

def preprocess(tweet, settings,nlp):

    flags = {"normalize": settings["preprocessing.normalize"],
             "remove": settings["preprocessing.remove"],
             "replace": settings["preprocessing.replace"]}
    
    # results will vary depending on the order/combination in which these are run

    for func in flags["replace"]:
        if eval(flags["replace"][func]):
            tweet =  eval("preprocessing.replace.{}(tweet)".format(func))
        #print(tweet)
        
    for func in flags["remove"]:
        if eval(flags["remove"][func]):
            tweet = eval("preprocessing.remove.{}(tweet)".format(func))
        #print(tweet)
        
    for func in flags["normalize"]:
        if eval(flags["normalize"][func]):
            tweet = eval("preprocessing.normalize.{}(tweet)".format(func))
        #print(tweet)

    # to lowercase
    if eval(settings["preprocessing.fixes"]["TO_LOWERCASE"]):
        tweet = tweet.lower()
    
    # TODO: hookup gensim phraser model here; check to see if Spanish option exists.
    # going to hold off on this for now since we are planning to use char-level ngrams anyways 
    # :. tokenization at the unigram level probably not as much of an issue 
    if eval(settings["preprocessing.fixes"]["FIND_PHRASES"]):
        pass
                
    # fix typos 
    # this flag is set to false in current config b/c I don't think this type of fix will be helpful given slang/Spanish
    # also, it's slow af. better to do it in batch mode after pruning vocab, if anything
    if eval(settings["preprocessing.fixes"]["FIX_TYPOS"]):
        tweet = fix_typos(tweet)

    # code snippet to do dependency parsing, the general format would be to replace every word 
    # with the tuple (word,head,dep_relation,word_pos,head_pos) with comma as the separator(We may
    # change it if necessary). The dep_relation, word_pos, head_pos are optional, depending on the setting
    # in {english,spanish}_config.ini.
    # This snippet is written by Chujun Song, feel free to contact him if you have any questions. 
    if eval(settings["preprocessing.dependency_parsing"]["DO_DEPENDENCY_PARSING"]):
        doc = nlp(tweet)
        splitted_tweet = tweet.split()
        deplist = []
        for token in doc:
            #deplist is hard coding, so do not change their position in deplist.
            deplist.append([token.text, token.head.text, token.dep_, token.pos_, token.head.pos_]
                )
        for i in range(len(splitted_tweet)):
            splitted_tweet[i] +=(','+ deplist[i][1])
        
        if eval(settings["preprocessing.dependency_parsing"]["ADD_RELATION"]):
            for i in range(len(splitted_tweet)):
                splitted_tweet[i] +=(','+deplist[i][2])
        if eval(settings["preprocessing.dependency_parsing"]["ADD_TOKEN_POS"]):
            for i in range(len(splitted_tweet)):
                splitted_tweet[i] +=(','+deplist[i][3])
        if eval(settings["preprocessing.dependency_parsing"]["ADD_HEAD_POS"]):
            for i in range(len(splitted_tweet)):
                splitted_tweet[i] +=(','+deplist[i][4])
        s = " "
        tweet = s.join(splitted_tweet)
                

    # We should get dependency parse trees before we do it if/when we pass this flag
    if eval(settings["preprocessing.fixes"]["REMOVE_STOPWORDS"]):
        
        lang = settings["paths"]["LANG"]
        tweet = ' '.join(x for x in tweet.split() if x not in eval("spacy.lang.{}.stop_words.STOP_WORDS".format(lang)))
        
    # another case for eval to catch the lang flag here: spacy.lang.en.stop_words.STOP_WORDS
    
    # TODO: lemmatize
    if eval(settings["preprocessing.fixes"]["LEMMATIZE"]):
        pass

    if len(tweet) == 0 and lang == "en":
        tweet = "<blank>"
    elif len(tweet) == 0 and lang == "es":
        tweet = "<blanco>"


    tweet_final = ''.join(word.strip() + " " for word in tweet.split())
    return tweet_final

def create_corpus(settings, df):
    
    spacy_lang = textacy.load_spacy_lang(settings["paths"]["SPACY_LANG"])
    corpus = textacy.Corpus(spacy_lang)
    corpus.add_records([row["text"], {"id":row["id"], "HS":row["HS"], "TR":row["TR"], "AG":row["AG"]}] for _, row in df.iterrows())
    
    return corpus

def preprocess_corpus(settings, orig_corpus):
    
    spacy_lang = textacy.load_spacy_lang(settings["paths"]["SPACY_LANG"])
    preproc_corpus = textacy.Corpus(spacy_lang)
    nlp = None
    if spacy_lang.lang=="en":
        nlp = spacy.load("en_core_web_sm")
    elif spacy_lang.lang =="es":
        nlp = spacy.load("es_core_news_sm")
    else:
        print("Unsupported Language")
        assert(0)
    preproc_corpus.add_records([preprocess(doc.text, settings,nlp), doc._.meta] for doc in orig_corpus.docs if len(str(doc.text)) > 0)

    return preproc_corpus

def write_preproc_to_file(settings, orig_filename, pp_corpus):
    
    preproc_dir = settings["paths"]["PP_DATA_DIR"]
    lang = settings["paths"]["LANG"]
    
    if ("dev" in orig_filename) or ("train" in orig_filename):
        subdir = "dev"
    else:
        subdir = "reference_test"

    #if not os.path.exists("{}/{}".format(preproc_dir, subdir)):
    if not os.path.exists(os.path.join(preproc_dir, subdir)):
        #os.makedirs("{}/{}".format(preproc_dir, subdir))
        os.makedirs(os.path.join(preproc_dir, subdir))
    
    #f = open("{}/{}/preproc_{}".format(preproc_dir, subdir, orig_filename),"w+")
    f = open(os.path.join(preproc_dir, subdir, "preproc_{}".format(orig_filename)), "w+")
    
    print("Writing preprocessed corpus to file{}".format(os.path.join(preproc_dir, subdir, "preproc_{}".format(orig_filename))))

    cols = ["id", "text", "HS", "TR", "AG"]
    f.write('\t'.join(cols) + "\n")
    #print("Writing preprocessed corpus to file: {}".format("{}/{}/preproc_{}".format(preproc_dir, subdir, orig_filename)))
    for doc in pp_corpus:
        meta = doc._.meta

        if "number" in meta:
            print(meta)

        if len(doc.text) == 0 and lang == "en":
            doc.text = "<blank>"
        elif len(doc.text) == 0 and lang == "es":
            doc.text = "<blanco>"

        # hacky thing to get rid of the lines causing error in the dev/train_es dataset. some problem in the raw file seems to be causing misalignment.
        # ideally solve so that dropping these lines is not necessary.
        if "number" in str(meta["HS"]) or "number" in str(meta["TR"]) or "number" in str(meta["AG"]) or "number" in str(meta["id"]):
            pass
        else:
            strs = [str(meta["id"]), doc.text, str(meta["HS"]), str(meta["TR"]), str(meta["AG"])]
            f.write('\t'.join(strs) + "\n")
    
    f.close()
    print("Done.")

def sanity_check_files(settings, orig_filename):
    preproc_dir = settings["paths"]["PP_DATA_DIR"]
    lang = settings["paths"]["LANG"]
    flag  = 0

    if ("dev" in orig_filename) or ("train" in orig_filename):
        subdir = "dev"
    else:
        subdir = "reference_test"

    df = pd.read_csv(os.path.join(preproc_dir, subdir, "preproc_{}".format(orig_filename)), header=0, sep="\t")

    for i, row in df.iterrows():

        if len(str(row['text'])) == 0 or ("number" in str(row['id'])):
            print("id is: ", row.id, "text is", row.text)
            flag = 1

    if flag == 0:
        print("No errors detected in file: {}".format(os.path.join(preproc_dir, subdir, "preproc_{}".format(orig_filename))))
    else:
        print("Errors detected in file: {}".format(os.path.join(preproc_dir, subdir, "preproc_{}".format(orig_filename))))

    return flag



def main(config, df, filename):
    
    md, hd, ud, ed, sd, emojid, nd = extraction_main(config, df, filename)
    
    corpus = create_corpus(config, df)
    ppc = preprocess_corpus(config, corpus)
    write_preproc_to_file(config, filename, ppc)
    
    return


if __name__=="__main__":

    # python -m spacy download en_core_web_sm
    # python -m spacy download es_core_web_sm
    # python -m spacy download en
    
    # You may need to modify these paths to match your directory structure, and/or OS. These will work for OSX/Linux.
    config = configparser.ConfigParser()
    config.read(sys.argv[1])
    lang = sys.argv[2]

    if lang == "es":

        try:
            df = pd.read_csv(sys.argv[2], header=0, sep="\t", encoding='utf-8')
        except:

            df = pd.read_csv(sys.argv[2], header=None, sep="\t", encoding='utf-8')
    else:
        try:
            df = pd.read_csv(sys.argv[2], header=0, sep="\t")
        except:

            df = pd.read_csv(sys.argv[2], header=None, sep="\t")

    print(df.shape)
    df.columns = ["id", "text", "HS", "TR", "AG"]
    
    # platform.system() gives you a way to switch on high-level OS type (e.g., Linux; Windows; Darwin)
    
    # this should still work if you're running a VM/Docker/terminal emulator from Windows machine (I think. lmk if not)
    filename = "{}".format(sys.argv[2]).split("/")[5]
    main(config, df, filename)
    flag = sanity_check_files(config, filename)



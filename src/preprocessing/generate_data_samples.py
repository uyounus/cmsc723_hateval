# %%

# coding: utf-8

# %%
import os
import random
import sys
from itertools import chain
import pandas as pd
import math
import platform


# %%
def get_files(root):
    
    #if not os.path.isdir("{}/sample_raw".format(root)):
    if not os.path.isdir(os.path.join(root, "sample_raw")):
        #os.makedirs("{}/sample_raw".format(root))
        os.makedirs(os.path.join(root, "sample_raw"))
        
        
    files_to_sample = []
        
    #for root, dirlist, filelist in os.walk("{}/raw".format(root)):
    for root, dirlist, filelist in os.walk(os.path.join(root, "raw")):

        for f in filelist:
            if f.endswith("tsv"):
                files_to_sample.append(os.path.join(root, f))
                
    return files_to_sample


def main(ROOT_DIR):

    # sample usage: python3 preprocessing/generate_data_samples.py ../data/

    files_to_sample = get_files(ROOT_DIR)
    print(files_to_sample)
    
    for f in files_to_sample:
        
        if "reference_test" in f:
        
            df = pd.read_csv(f, sep="\t", header=None)
            df.columns = ["id", "text", "HS", "TR", "AG"]
            
        else:
            df = pd.read_csv(f, sep="\t", header=0)
        
        sample_df = df.sample(n = math.ceil(df.shape[0]*0.005), random_state= random.seed(357))
        
        if platform.system() == "Windows":
            subdir = f.split("\\")[3]
            new_name = "{}_{}.tsv".format(f.split("\\")[4].split(".tsv")[0], "sample")
        
        else:
            subdir = f.split("/")[3]
            new_name = "{}_{}.tsv".format(f.split("/")[4].split(".tsv")[0], "sample")
        
        
#         if not os.path.exists("{}/sample_raw/{}".format(ROOT_DIR, subdir)):
#             os.makedirs("{}/sample_raw/{}".format(ROOT_DIR, subdir))

#         sample_df.to_csv("{}/sample_raw/{}/{}".format(ROOT_DIR, subdir, new_name), sep="\t")

        if not os.path.exists(os.path.join(ROOT_DIR, "sample_raw", subdir)):
            os.makedirs(os.path.join(ROOT_DIR, "sample_raw", subdir))

        # bug fix to make sure we don't write row numbers
        sample_df.to_csv(os.path.join(ROOT_DIR, "sample_raw", subdir, new_name), sep="\t", index=False)

# %%
if __name__=="__main__":
    main(sys.argv[1])


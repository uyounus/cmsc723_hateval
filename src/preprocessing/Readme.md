## Preprocessing

### Dependency parsing
Dependency parsing is added as one of the steps in preprocessing. Basicsly, it will take a input sentence as
input and replace each word in the sentence with the tuple (word,head,dep_relation,word_pos,head_pos) with comma as the separator. This way of replacement is inspired by the paper [Dependency-Guided LSTM-CRF for Named Entity Recognition](https://arxiv.org/abs/1909.10148) that appeared in EMNLP2019.
The dep_relation, word_pos, head_pos are optional, depending on the setting in {english,spanish}_config.ini.

For example, the sentence "I am a cat" will be replace "with I,am,nsubj am,am,ROOT a,cat,det cat,am,attr"(In this case, the tuple is (word,head,dep_relation)). You may want to change the configuration in {english,spanish}_config.ini.[preprocessing.dependency_parsing].
You may want to run `python -m spacy download en_core_web_sm` or `python -m spacy download es_core_news_sm`
for downloading the missing modules.


If you have any questions, please contact Chujun Song

### Word Embeddings
We will start with [fasttext](https://fasttext.cc/). You may want to downlaod it following this [link](https://fasttext.cc/docs/en/support.html#building-fasttext-python-module) and we can also try the other word embeddings if time permits.

All the data files will be in this (**And this is NOT our team drive!!!**)[google drive folder](https://drive.google.com/drive/u/1/folders/1C0W8Oka4s4lQ5N8jDw0Cn_efFUKcNt6e)(**And this is NOT our team drive!!!**).

#### English Only

##### crawl-300d-2M

The embeddings in [crawl-300d-2M.vec](https://fasttext.cc/docs/en/english-vectors.html#download-pre-trained-word-vectors)(crawl-300d-2M.vec.zip in google drive) is 2 million 300 dimension vectors trained on Common Crawl (600B tokens). **And this is Enlish only!!!**. It can be loaded using the following code:

```python
import io

def load_vectors(fname):
    fin = io.open(fname, 'r', encoding='utf-8', newline='\n', errors='ignore')
    n, d = map(int, fin.readline().split())
    data = {}
    for line in fin:
        tokens = line.rstrip().split(' ')
        data[tokens[0]] = map(float, tokens[1:])
    return data

#PATH is the actual path for crawl-300d-2M.vec, you may want to change it depending on there you put it.
PATH ="../data/crawl-300d-2M.vec"
load_vectors(PATH)
```

Because of the size of the doc, it took about 30 minutes to load the vectors on my laptop(If you think this is too long, you can start with the following embeddings first, which took less than one minute to load), but this is not a problem since we only have a small vocabulary, we can only save the vector of words in our
corpus in another file and use that file instead. **And I highly suggest whoever uses the embeddings first to do so**.(BTW, if any one do that, please also upload the smaller data file to google drive so all of us can use that, and you can change this paragraph accordingly). 

####  For both English and Spanish

##### [Wiki word vectors](https://fasttext.cc/docs/en/pretrained-vectors.html)
The data file's name for English is wiki.en.zip while data file's name for Spanish is wiki.es.zip
These are 300 dimension vectors trained on Wikipedia using the skip-gram model. 
This is much faster to load(less than 1 min on my laptop). It can be loaded using the following code:

```python
import fasttext

model = fasttext.load_model("../data/wiki.en/wiki.en.bin")
model.get_word_vector("the")
```

import numpy as np
import pandas as pd
from os.path import join
from src.baseline.utils import get_label, list2num, all_metrics
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.svm import SVC 
import timeit
from collections import Counter


def get_label_separate(data_pd, task_b=False):
    """
    input: csv file
    output: labels associated with each twitter
    note: label list: as a list of tuples, and the most frequent label
    """
    if task_b:

        subset = data_pd[data_pd.loc[:,"HS"]==1]

        all_label = [tuple(item) for item in data_pd.values.tolist()]
        mf_labelA = Counter(all_label[0]).most_common(1)
        mf_labelB = Counter(all_label[1]).most_common(1)
        mf_labelC = Counter(all_label[2]).most_common(1)
        print("Label A: {}, Label B: {}, Label C: {}", mf_labelA, mf_labelB, mf_labelC)
        
        all_labelS = [tuple(item) for item in subset.values.tolist()]
        mf_labelAS = Counter(all_labelS[0]).most_common(1)
        mf_labelBS = Counter(all_labelS[1]).most_common(1)
        mf_labelCS = Counter(all_labelS[2]).most_common(1)
        print("Label AS: {}, Label BS: {}, Label CS: {}", mf_labelAS, mf_labelBS, mf_labelCS)
        
        mf_result = Counter(all_label).most_common(1)
        mf_label = mf_result[0][0]
        mf_cn = mf_result[0][1]
        print("There are %d labels in the dataset, and the most frequent label is (%d, %d, %d), which occured %d times"%(len(all_label), mf_label[0], mf_label[1], mf_label[2], mf_cn))
    else:
        #only take the hate speech label
        all_label = [item[0] for item in data_pd.values.tolist()]
        mf_result = Counter(all_label).most_common(1)
        mf_label = mf_result[0][0]
        mf_cn = mf_result[0][1]

        print(np.unique(all_label ), "mf label", mf_label, "mf cn", mf_cn)
        print("There are %d labels in the dataset, and the most frequent label is %d, which occured %d times"%(len(all_label), mf_label, mf_cn))
    return all_label, mf_label


def run_mfc(train_lb_df, test_lb_df, task_b):
    """
    most frequent label classifier:
    input: training labels and test labels
    output: all metrics
    """
    train_label, mf_label = get_label_separate(train_lb_df, task_b)
    test_label, _ = get_label_separate(test_lb_df, task_b)
    pred_ls = [mf_label] * len(test_label)
    res = all_metrics(pred_ls, test_label, task_b)
    
    return res


def run_tfidf_svm(train_dat_df, test_dat_df, train_lb_df, test_lb_df, task_b):
    """
    linear SVM classifiers
    input: train_data_df, test_data_df, train_label, test_label
    output: all metrics
    """
    start_time = timeit.default_timer()
    vectorizer = TfidfVectorizer()
    train_lb, _ = get_label(train_lb_df, task_b)
    test_lb, _ = get_label(test_lb_df, task_b)
    train_vec = vectorizer.fit_transform(train_dat_df)
    feature_names = vectorizer.get_feature_names()
    #ratio can be adjusted smaller for test
    ratio = len(feature_names)
    train_mat = train_vec.todense().tolist()[:ratio]
    train_lb = train_lb[:ratio]
    test_vec = vectorizer.transform(test_dat_df)
    test_mat = test_vec.todense().tolist()[:ratio]
    test_lb = test_lb[:ratio]
    print("For TFIDF, %0.3f seconds elapsed."%(timeit.default_timer() - start_time))

    if task_b:
        start_time = timeit.default_timer()
        clf1 = SVC(kernel='linear')
        label_1 = [items[0] for items in train_lb]
        clf1.fit(train_mat, label_1)
        pred_label_1 = clf1.predict(test_mat)

        clf2 = SVC(kernel='linear')
        label_2 = [items[1] for items in train_lb]
        clf2.fit(train_mat, label_2)
        pred_label_2 = clf2.predict(test_mat)
        
        clf3 = SVC(kernel='linear')
        label_3 = [items[2] for items in train_lb]
        clf3.fit(train_mat, label_3)
        pred_label_3 = clf3.predict(test_mat)
        pred_lb = []
        # pred lb is [[y11,y12,y13], [y11,y22, y23], ...]
        for i in range(len(pred_label_1)):
            tmp = [pred_label_1[i], pred_label_2[i], pred_label_3[i]]
            pred_lb.append(tmp) 
        print("For TFIDF SVM")
        res = all_metrics(pred_lb, test_lb, task_b)
        print("During SVM training, %0.3f seconds elapsed."%(timeit.default_timer() - start_time))
        return res
    else:
        start_time = timeit.default_timer()
        clf = SVC(kernel='linear')
        clf.fit(train_mat, train_lb)
        pred_lb = clf.predict(test_mat)
        print("For TFIDF SVM")
        res = all_metrics(pred_lb, test_lb, task_b)
        print("During SVM training, %0.3f seconds elapsed."%(timeit.default_timer() - start_time))
        return res


if __name__ == "__main__":
    # parameters
    language = "en"
    if language == "en":
        print("In English")
    else:
        print("In Spanish")
    fdir = "./../../data/preprocessed/"
    train_data_f = join(fdir, "dev/preproc_train_%s.tsv"%language)
    test_data_f = join(fdir, "reference_test/preproc_%s.tsv"%language)
    train_lb_df = pd.read_csv(train_data_f, sep='\t', header=0).iloc[:,2:5]
    test_lb_df = pd.read_csv(test_data_f, sep='\t', header=0).iloc[:,2:5]
    task_b = False
    train_dat_df = pd.read_csv(train_data_f, sep='\t', header=0).iloc[:,1].values.tolist()
    test_dat_df = pd.read_csv(test_data_f, sep='\t', header=0).iloc[:,1].values.tolist()
    #15k in vocabulary

    # call functions
    run_mfc(train_lb_df, test_lb_df, task_b) 
    #run_tfidf_svm(train_dat_df, test_dat_df, train_lb_df, test_lb_df, task_b)





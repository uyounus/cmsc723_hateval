from mlxtend.plotting import plot_confusion_matrix
from sklearn.metrics import  confusion_matrix
import matplotlib.pyplot as plt
import numpy as np
import pickle

def show_confusion_matrix(path):
    try:
        with open(path,"rb") as fp:
            real_ls = pickle.load(fp)
            pred_ls = pickle.load(fp)
    except FileExistsError:
        print("file not exists")
        return

    binary = confusion_matrix(real_ls, pred_ls)
    # print(binary)

    fig, ax = plot_confusion_matrix(conf_mat=binary,
                                    show_absolute=True,
                                    show_normed=True,
                                    colorbar=True)
    plt.show()
path = "../../data/neural_results/en/a/en_HS.pickle"
show_confusion_matrix(path)
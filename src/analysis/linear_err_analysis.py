import numpy as np
import pandas as pd
from os.path import join
from collections import Counter


def get_inter(f1,f2):
    """
    get intersection of two files
    """
    f1_df = pd.read_csv(f1, sep='\t')['n-gram'].tolist()
    f2_df = pd.read_csv(f2, sep='\t')['n-gram'].tolist()
    inter_ls = list(set(f1_df).intersection(f2_df))
    return inter_ls

def get_mislabel(pred_label, real_label, class_name, out_dir):
    """
    input: predicted labels, real labels, class_name
    output: the index (0-indexed) of false negative and false positive tweets 
    """

    assert(len(pred_label) == len(real_label))
    tp_ls = []
    tn_ls = []
    fp_ls =[]
    fn_ls =[]
    for i in range(len(pred_label)):
        if pred_label[i] == real_label[i]:
            if real_label[i]==1:
                tp_ls.append(i)
            else:
                tn_ls.append(i)
        else:
            if pred_label[i] == 1:
                #false positive
                fp_ls.append(i)
            else:
                fn_ls.append(i)
    with open(join(out_dir,"%s_fp.txt"%class_name),'w') as out_f:
        for fl in fp_ls:
            out_f.write(str(fl) + '\n')
    with open(join(out_dir,"%s_fn.txt"%class_name),'w') as out_f2:
        for nl in fn_ls:
            out_f2.write(str(nl) + '\n')
    
    with open(join(out_dir,"%s_tp.txt"%class_name),'w') as out_f3:
        for tl in tp_ls:
            out_f3.write(str(tl) + '\n')
    with open(join(out_dir,"%s_tn.txt"%class_name),'w') as out_f4:
        for tn in tn_ls:
            out_f4.write(str(tn) + '\n')
    """
    ## Get intersections
    inter_posi = list(set(tp_ls).intersection(fp_ls))
    inter_nega = list(set(tn_ls).intersection(fn_ls))

    with open(join(out_dir,"%s_tp_fp_intersection.txt"%class_name),'w') as out_f5:
        for tl in inter_posi:
            out_f5.write(str(tl) + '\n')

    with open(join(out_dir,"%s_tn_fn_intersection.txt"%class_name),'w') as out_f6:
        for tn in inter_nega:
            out_f6.write(str(tn) + '\n')
    """

def get_ngram(tweet_df, index, classname, class_type, n, out_dir, top_perc):
    """
    input: a list of tweets, a list of index of selection, top_perc: store show top X percent of n-grams
    output: a dataframe of ranked n-gram frequency
    """
    # select those tweets
    sele_tweet = [tweet_df[i] for i in index]
    all_ngram = []
    if n==1:
        for st in sele_tweet:
            all_word = st.split(' ')
            all_ngram.extend(all_word)
    else:
        raise NotImplementedError
    cn_dict = Counter(all_ngram)
    most_common = cn_dict.most_common(int(len(sele_tweet)*top_perc))
    mc_term = []
    mc_cnt = []
    for m in range(len(most_common)):
        term = most_common[m][0]
        cnt = most_common[m][1]
        #print("No.%d: %s occured %d times"%(m+1, term, cnt))
        mc_term.append(term)
        mc_cnt.append(cnt)
    comb = list(zip(mc_term, mc_cnt))
    mc_df = pd.DataFrame(data=comb, columns=['n-gram','count'])
    mc_df.to_csv(join(out_dir, "top_%dpercent_tweets_%s_%s_ngram_%d.csv"%(top_perc*100, classname, class_type, n)),sep='\t')


      

if __name__ == "__main__":
    label_dir = "../../data/nonneural_labels"
    pred_f = join(label_dir,"en_pred.csv")
    real_f = join(label_dir,"en_label.csv")
    tweet_f = join("../../data/preprocessed/reference_test","preproc_en.tsv")
    class_ls = ['HS', 'TR', 'AG']
    # false negative and false positive
    class_type = ['fn','fp','tn','tp']
    # n-gram
    n = 1
    top_perc=0.1
    pred_df = pd.read_csv(pred_f, sep='\t')
    real_df = pd.read_csv(real_f, sep='\t')
    
    for cl in class_ls:
        pred_label =  pred_df[cl]
        real_label = real_df[cl]
        get_mislabel(pred_label.tolist(), real_label.tolist(), cl, label_dir)
    
    tweet_df = pd.read_csv(tweet_f, sep='\t').iloc[:,1].values.tolist()
    for cl in class_ls:
        for ms in class_type:
            index_f = join(label_dir, cl + "_" + ms + '.txt')
            with open(index_f) as ind_f:
                ind_ls = [int(itm) for itm in ind_f.read().splitlines()]
            get_ngram(tweet_df, ind_ls, cl, ms, n, label_dir, top_perc)
    
    bundle = [('fp','tp'), ('fn','tn')]
    for cl in class_ls:
        for bd in bundle:
            fname_1 = join(label_dir, "top_%dpercent_tweets_%s_%s_ngram_%d.csv"%(top_perc*100, cl, bd[0], n))
            fname_2 = join(label_dir, "top_%dpercent_tweets_%s_%s_ngram_%d.csv"%(top_perc*100, cl, bd[1], n))
            inter_ls = get_inter(fname_1, fname_2)
            with open(join(label_dir,"intersection_%s_%s_%s_ngram_%d.txt"%(cl, bd[0], bd[1], n)),'w') as out_f:
                for il in inter_ls:
                    out_f.write(str(il)+'\n')


            

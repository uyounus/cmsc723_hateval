#!/usr/bin/python
import pandas as pd
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt 


def run():
	method_type = ['Baselines','Non-neural models','Neural networks']
	method_class = ['official', 'n-gram', 'non-NN','NN']
	task_lang = ['Task-A-English','Task-B-English','Task-A-Spanish','Task-B-Spanish']
	
	official=[0.602,0.594,0.671,0.716]
	ngram=[0.589,0.589,0.708,0.725]
	nonNN= [0.493,0.605,0.658,0.697]
	nn=[0.626,0.535,0.672,0.632]

	all_F1 = official + ngram + nonNN + nn
	all_tasklang = task_lang * len(method_class)
	all_type = [method_type[0]]*2*4 + [method_type[1]]*1*4+[method_type[2]]*1*4
	all_class = []
	for items in method_class:
		all_class += [items] * 4
	
	long_df = [all_F1, all_tasklang, all_type, all_class]
	short_df = []
	for i in range(len(long_df[0])):
		tmp =[long_df[0][i], long_df[1][i], long_df[2][i], long_df[3][i]]
		short_df.append(tmp)
	
	df_all = pd.DataFrame(short_df, columns = ['macro-F1-score', 'task-language', 'Type', 'Model']) 
	df_all['F1'] = [float(nums) for nums in df_all['macro-F1-score']]
	fig, ax = plt.subplots()
	g = sns.catplot(x="Model", y="macro-F1-score", col="task-language",hue="Type",col_wrap=2,kind="bar",\
		data=df_all)
	g.set_xticklabels(['official', 'n-gram', 'non-NN','NN'])
	plt.savefig("hatsoff_results.png")

if __name__ == "__main__":
	run()

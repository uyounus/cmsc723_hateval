import numpy as np
import pandas as pd
import umap
from sklearn.cluster import KMeans
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.manifold import TSNE
from sklearn.decomposition import PCA
from sklearn.preprocessing import StandardScaler
from sklearn.pipeline import Pipeline

def vis_pca(X, y_label, title):
    """
    PCA
    """
    pipeline = Pipeline([('scaling', StandardScaler()), ('pca', PCA(n_components=2))])
    X_embedded = pipeline.fit_transform(X)
    sns.scatterplot(X_embedded[:,0], X_embedded[:,1], legend='full',  hue=y_label, palette="Set1")
    plt.title('Sentence embeddings visualized by PCA')
    plt.savefig('figures/pca_%s.pdf'%title)
    plt.show()

def vis_tsne(X,y_label, title):
    """
    visualize by tSNE
    """
    X_embedded = TSNE(n_components=2, random_state=1234).fit_transform(X)
    sns.scatterplot(X_embedded[:,0], X_embedded[:,1], legend='full',  hue=y_label, palette="Set1")
    plt.title('Sentence embeddings visualized by t-SNE')
    plt.savefig('figures/tSNE_%s.pdf'%title)
    plt.show()

def vis_umap(X, y_label,title):
    """
    visualize by UMAP
    """
    reducer = umap.UMAP(random_state=1234)
    X_embedded = reducer.fit_transform(X)
    #markers = ('o', 'v', 's', 'p', '*', 'h', 'H', 'D', 'd', 'P', 'X')
    sns.scatterplot(X_embedded[:,0], X_embedded[:,1], legend='full',  hue=y_label, palette="Set1")
    plt.title('Sentence embeddings visualized by UMAP')
    plt.savefig('figures/UMAP_%s.pdf'%title)
    plt.show()
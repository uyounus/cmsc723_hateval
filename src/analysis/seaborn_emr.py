#!/usr/bin/python
import pandas as pd
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt 


def run():
	method_type = ['Baselines','Non-neural models','Neural networks']
	method_class = ['official', 'n-gram', 'non-NN','NN']
	task_lang = ['Task-B-English','Task-B-Spanish']
	
	official=[0.578,0.587]
	ngram=[0.391,0.650]
	nonNN= [0.324,0.632]
	nn=[0.395,0.542]

	all_F1 = official + ngram + nonNN + nn
	all_tasklang = task_lang * len(method_class)
	all_type = [method_type[0]]*2*2 + [method_type[1]]*1*2+[method_type[2]]*1*2
	all_class = []
	for items in method_class:
		all_class += [items] * 2
	
	long_df = [all_F1, all_tasklang, all_type, all_class]
	short_df = []
	for i in range(len(long_df[0])):
		tmp =[long_df[0][i], long_df[1][i], long_df[2][i], long_df[3][i]]
		short_df.append(tmp)
	
	df_all = pd.DataFrame(short_df, columns = ['Exact_Match_Ratio', 'task-language', 'Type', 'Model']) 
	df_all['F1'] = [float(nums) for nums in df_all['Exact_Match_Ratio']]
	fig, ax = plt.subplots()
	g = sns.catplot(x="Model", y="Exact_Match_Ratio", col="task-language",hue="Type",col_wrap=2,kind="bar",\
		data=df_all)
	g.set_xticklabels(['official', 'n-gram', 'non-NN','NN'])
	plt.savefig("hatsoff_results_emr.png")

if __name__ == "__main__":
	run()

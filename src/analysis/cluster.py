import numpy as np
import pickle
# import fasttext
import pandas as pd
from vis_util import vis_pca, vis_tsne, vis_umap


def trans_dict(id_class):
    """
    input: id_class: key: fn/fp/tn/tp, values: id
    output: transpose_id_class, key: id, values:  fn/fp/tn/tp
    """
    trans_id_class = {}
    labels = ['TP', 'TN', 'FP', 'FN']
    for ls in labels:
        ids_ls = id_class[ls]
        for ids in ids_ls:
            trans_id_class[ids] = ls
    return trans_id_class

def get_embed(text_path,res_path,stop_word_list,embedding_path, embed_dim):

    if embedding_path.endswith("txt"):
        embeddings_index = {}
        for i, line in enumerate(open(embedding_path)):
            val = line.split()
            embeddings_index[val[0]] = np.asarray(val[-embed_dim:], dtype='float32')
    else:
        # embeddings for .bin is not supported
        assert(0)
    # model = fasttext.load_model(embedding_path)
    df = pd.read_csv(text_path,sep="\t")
    id_column = df['id']
    text_column = df['text']
    id_embed_mapping = {}
    for i in range(len(id_column)):
        id_embed_mapping[id_column[i]] = sentence2embed(text_column[i],embeddings_index,stop_word_list, embed_dim)
    # label_column = df['label']
    with open(res_path,"rb") as fp:
        real_ls = pickle.load(fp)
        pred_ls = pickle.load(fp)
    # mapping_array: from id to real to pred, each row would be (id,real,pred)
    mapping_array = np.transpose(np.array([id_column,real_ls,pred_ls]))
    # id_class: mapping from lable to id
    id_class = {'TP':[],'TN':[],'FP':[],'FN':[]}
    for i in range(mapping_array.shape[0]):
        # for j in range(mapping_array.shape[1]):
        if int(mapping_array[i][1])==1 and int(mapping_array[i][2])==1:
            id_class['TP'].append(int(mapping_array[i][0]))
        elif int(mapping_array[i][1])==1 and int(mapping_array[i][2])==0:
            id_class['FN'].append(int(mapping_array[i][0]))
        elif int(mapping_array[i][1])==0 and int(mapping_array[i][2])==0:
            id_class['TN'].append(int(mapping_array[i][0]))
        elif int(mapping_array[i][1])==0 and int(mapping_array[i][2])==1:
            id_class['FP'].append(int(mapping_array[i][0]))
        else:
            assert(0)
    return id_embed_mapping, id_class
    
def remove_stop_words(tweet,stop_word_list):
    for word in stop_word_list:
        tweet = tweet.replace(word,'')
    return tweet

def sentence2embed(tweet,model,stop_word_list, embed_dim):
    tweet = remove_stop_words(tweet,stop_word_list)
    # embed_len  = model.get_dimension()
    vec_list = []
    for word in tweet.split():
        try:
            word_embed = model[word]
            if np.isnan(word_embed).all():
                with open('../nonneural/avg_embed.txt') as in_f:
                    word_embed = [float(item) for item in in_f.readline().strip('\n').split(' ')][:embed_dim]
        except:
            # for out-of-vocabulary words, see https://stackoverflow.com/questions/49239941/what-is-unk-in-the-pretrained-glove-vector-files-e-g-glove-6b-50d-txt
            with open('../nonneural/avg_embed.txt') as in_f:
                word_embed = [float(item) for item in in_f.readline().strip('\n').split(' ')][:embed_dim]
        vec_list.append(word_embed)
    data = np.array(vec_list)
    return np.average(data, axis=0)

def main():
    text_path = "../../data/preprocessed/reference_test/preproc_en.tsv"
    res_path = "../../data/neural_results/es/a/es_HS.pickle"
    #embedding_path = "../../data/glove.twitter.27B/glove.twitter.27B.200d.txt"
    embedding_path="../../data/wiki.es.vec.txt"
    #stop_word_list = ['user','tag','in','emoji','number']
    stop_word_list =[" "]
    embed_dim=300
    id_embed_mapping, id_class = get_embed(text_path,res_path,stop_word_list,embedding_path,embed_dim)
    trans_id_class = trans_dict(id_class)
    assert(len(trans_id_class.keys())==len(id_embed_mapping.keys()))
    pX_embed = []
    nX_embed = []
    py_label = []
    ny_label = []
    id_list = trans_id_class.keys()
    for ids in id_list:
        label = trans_id_class[ids]
        if label=="TP" or label=="FP":
            tmp = id_embed_mapping[ids]
            pX_embed.append(tmp)
            py_label.append(label)
        else:
            tmp = id_embed_mapping[ids]
            nX_embed.append(tmp)
            ny_label.append(label)
    
    pX_embed = np.array(pX_embed)
    nX_embed = np.array(nX_embed)

    vis_pca(pX_embed, py_label,"tp_fp")
    vis_tsne(pX_embed, py_label,"tp_fp")
    vis_umap(pX_embed, py_label,"tp_fp")

    vis_pca(nX_embed, ny_label,"tn_fn")
    vis_tsne(nX_embed, ny_label,"tn_fn")
    vis_umap(nX_embed, ny_label,"tn_fn")

if __name__ =="__main__":
    main()

        

    
    
            



    



import matplotlib.pyplot as plt
import pandas as pd
from matplotlib.ticker import FuncFormatter
import numpy as np

from mlxtend.plotting import plot_confusion_matrix
from sklearn.metrics import  confusion_matrix
import pickle

from sklearn.feature_extraction.text import CountVectorizer



    




def get_label_and_run(text_path,res_path,ngram_range,top_k,stop_word_list):
    df = pd.read_csv(text_path,sep="\t")
    id_column = df['id']
    text_column = df['text']
    # label_column = df[label]
    id_text_mapping = {}
    for i in range(len(id_column)):
        id_text_mapping[id_column[i]] = remove_stop_words(text_column[i],stop_word_list)
    # label_column = df['label']
    with open(res_path,"rb") as fp:
        real_ls = pickle.load(fp)
        pred_ls = pickle.load(fp)
    mapping_array = np.transpose(np.array([id_column,real_ls,pred_ls]))
    id_class = {'TP':[],'TN':[],'FP':[],'FN':[]}
    for i in range(mapping_array.shape[0]):
        # for j in range(mapping_array.shape[1]):
        if int(mapping_array[i][1])==1 and int(mapping_array[i][2])==1:
            id_class['TP'].append(int(mapping_array[i][0]))
        elif int(mapping_array[i][1])==1 and int(mapping_array[i][2])==0:
            id_class['FN'].append(int(mapping_array[i][0]))
        elif int(mapping_array[i][1])==0 and int(mapping_array[i][2])==0:
            id_class['TN'].append(int(mapping_array[i][0]))
        elif int(mapping_array[i][1])==0 and int(mapping_array[i][2])==1:
            id_class['FP'].append(int(mapping_array[i][0]))
        else:
            assert(0)
    for item in id_class.items():
        print(item[0])
        cal_nrams(item[1],id_text_mapping,ngram_range,top_k)



def cal_nrams(id_list,id_text_mapping,ngram_range,tok_k):
    corpus_list = []
    for id in id_list:
        corpus_list.append(id_text_mapping[id])
    ngram_vectorizer = CountVectorizer(analyzer='word', ngram_range=ngram_range)
    counts = ngram_vectorizer.fit_transform(corpus_list)
    ngram_vectorizer.get_feature_names() 
    document_term_matrix = counts.toarray()
    index_sum_dict = {}
    sum_all = document_term_matrix.sum()
    for i in range(document_term_matrix.shape[1]):
        index_sum_dict[i] = np.sum(document_term_matrix[:,i])/sum_all
    index_sum_dict = {k: v for k, v in sorted(index_sum_dict.items(), key=lambda item: item[1],reverse=True)}
    # return index_sum_dict
    index2word = {k:v for v,k in ngram_vectorizer.vocabulary_.items()}
    cnt = 0
    for index_sum in index_sum_dict.items():
        if cnt < tok_k:
            print(index2word[index_sum[0]])
            print(index_sum[1])
            cnt= cnt+1
        else:
            break
def remove_stop_words(tweet,stop_word_list):
    for word in stop_word_list:
        tweet = tweet.replace(word,'')
    return tweet
def main():
    text_path = "../../data/preprocessed/reference_test/preproc_en.tsv"
    res_path = "../../data/neural_results/en/a/en_HS.pickle"
    ngram_range = (3,3)
    top_k = 5
    stop_word_list = ['user','tag','in','emoji','number']

    get_label_and_run(text_path,res_path,ngram_range,top_k,stop_word_list)
if __name__ =="__main__":
    main()

        

    
    
            



    


# label_path =  "/Users/song/Desktop/NLProj/hateval/data/preprocessed/reference_test/preproc_en.tsv"
# real_path = "/Users/song/Desktop/NLProj/hateval/data/neural_results/en/b/batch_0/en_AG.pickle"
# compare(label_path,real_path)
# ngram_vectorizer = CountVectorizer(analyzer='word', ngram_range=(2, 2))
# counts = ngram_vectorizer.fit_transform(['hello words', 'll  ls lw lw wprds'])
# ngram_vectorizer.get_feature_names() 
# print(ngram_vectorizer.get_feature_names())
# print(counts.toarray())
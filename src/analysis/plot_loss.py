import matplotlib.pyplot as plt
import pandas as pd
from matplotlib.ticker import FuncFormatter
import numpy as np



def to_percent(temp, position):
    return '%1.00f'%(100*temp) + '%'

def draw_plot(data_path):
    df = pd.read_csv(data_path)
    # print (df.columns)
    epochs = df['epoch']
    train_loss = df['avg_train_loss']
    validation_loss = df['avg_valid_loss']
    test_loss = df['avg_test_loss']
    # print(train_loss)




    # round_list = [5,10,15,20]
    # n25f3=[0.001,0.129,0.511,0.805]
    # n50f4=[0.012,0.378,0.859,0.974]
    # n100f5=[0.041,0.695,0.984,0.999]
    # n25f0=[0,0.006,0.006,0.008]
    # n50f0=[0.006,0.011,0.009,0.012]
    # n100f0=[0.015,0.017,0.012,0.014]
    plt.xticks(epochs)
    plt.plot(epochs, train_loss, color='r', marker='1', label='train_loss')
    plt.plot(epochs, validation_loss, color='g', marker='2', label='validation_loss')
    plt.plot(epochs, test_loss, color='b', marker='3', label='test_loss')

    # plt.plot(round_list, n25f0, '--', color='r', marker='1', label='n=25,f=0')
    # plt.plot(round_list, n50f0, '--', color='g', marker='2', label='n=50,f=0')
    # plt.plot(round_list, n100f0, '--', color='b', marker='3', label='n=100,f=0')

    # plt.legend()
    # plt.gca().yaxis.set_major_formatter(FuncFormatter(to_percent))
    plt.xlabel('epoch')
    # plt.ylim([0,1])
    plt.ylabel('loss')
    plt.legend()
    plt.show()

path = "/Users/song/Desktop/NLProj/hateval/data/loss_files/en/a/HS_bilstm_en_gloveTwitter27B200d_25_base_12_14_16:30:19.txt"
draw_plot(path)

from src.nn.rnnKeras import Model
import torch
PATH = "../../data/model/0.614_HS_bilstm_en_gloveTwitter27B200d_25_base_12_14_16_32_39.pickle"
def load_model(PATH):
    model = torch.load(PATH)
    model.eval()
    return model


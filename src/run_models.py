#!/usr/bin/env python3
import pandas as pd
import numpy as np
import os
from collections import OrderedDict, Counter
import re
import textacy
from textacy import preprocessing
import spacy
import configparser
import random
import emoji
from spellchecker import SpellChecker
import sys
import platform
import pickle
import pandas as pd
# %run baseline/offical_baseline.py
# %run baseline/utils.py
# %run nonneural/nonneural.py
# %run nonneural/utils.py
# %run augmenting_features/gen_augmenting_features.py
# comment out these last 5 lines and uncomment the three lines below if running as a py script 
from src.baseline import utils, offical_baseline
from src.nonneural import nonneural, utils
from src.augmenting_features import gen_aug_features

# +
# for reference; will use later to create a main 
# def create_results_df():
    
#     dev_datasets = ["preproc_dev_en.tsv", "preproc_dev_es.tsv", "preproc_train_en.tsv", "preproc_train_es.tsv"]
#     ref_test_datasets = ["preproc_en.tsv", "preproc_es.tsv"]
    
#     tasks = ["a", "b"]
    
#     dataset_lang = ["en", "es", "joint"]
    
#     input_matrices = ["label_counts", "tf-idf", "ngrams", "es_ft_embeds", "en_ft_embeds", "joint_ft_embeds"]
    
#     augmenting_features = ["none", "lexical", "graph"]
    
#     df = pd.DataFrame(columns=["task", "dataset", "dataset_lang",
#                          "input_matrix", "model", "macro_avg_f1", "accuracy", "precision", "recall", "emr"])


# -
def format_results(task:str, lang:str, input_X:str, aug_features:list, model_category:str, model_name:str, results_dict:dict):
    
    if task == "a":
        
        return [task, 
                "ref_test_preproc_{}".format(lang),
                lang,
                input_X,
                " ,".join(str(x) for x in aug_features),
                model_category,
                model_name,
                results_dict["macro_avg_f1"],
                results_dict["acc"],
                results_dict["pr"],
                results_dict["rs"],
                None]
    else:
        
        return  [task, 
                "ref_test_preproc_{}".format(lang),
                lang,
                input_X,
                " ,".join(str(x) for x in aug_features),
                model_category,
                model_name,
                results_dict["macro_avg_f1"],
                None,
                None,
                None,
                results_dict["emr"]]



def run_baselines_return_results(languages=["en", "es"], tasks =["a","b"]):

    baseline_results = pd.DataFrame(columns=["task", "dataset", "dataset_lang",
                         "input_matrix", "aug_features", "model_cat", "model", "macro_avg_f1", "accuracy", "precision", "recall", "emr"])
    
    task_b_switch = False
    counter = 0
    
    for task in tasks:
        
        print("Task: ", task.upper(), "\n")  
        
        if task == "b":
            task_b_switch = True
        
        for language in languages:
            
            print("Language: {} \n".format("English" if language == "en" else "Spanish"))
            
            fdir = "../data/preprocessed/"
            train_data_f = os.path.join(fdir, "dev/preproc_train_%s.tsv"%language)
            test_data_f = os.path.join(fdir, "reference_test/preproc_%s.tsv"%language)
            train_lb_df = pd.read_csv(train_data_f, sep='\t', header=0).iloc[:,2:5]
            test_lb_df = pd.read_csv(test_data_f, sep='\t', header=0).iloc[:,2:5]
            train_dat_df = pd.read_csv(train_data_f, sep='\t', header=0).iloc[:,1].values.tolist()
            test_dat_df = pd.read_csv(test_data_f, sep='\t', header=0).iloc[:,1].values.tolist()
            
            # call functions            
            mfc_res = offical_baseline.run_mfc(train_lb_df, test_lb_df, task_b_switch)
            
            baseline_results.loc[counter, :] = format_results(task=task,
                                                              lang=language,
                                                              input_X="labels_only",
                                                              aug_features=[None],
                                                              model_category="baseline",
                                                              model_name="mfc_baseline",
                                                              results_dict=mfc_res)
            counter +=1 
            
            svm_res = offical_baseline.run_tfidf_svm(train_dat_df, test_dat_df, train_lb_df, test_lb_df, task_b_switch)

            baseline_results.loc[counter, :] = format_results(task=task,
                                                      lang=language,
                                                      input_X="tfidf",
                                                      aug_features=[None],
                                                      model_category="baseline",
                                                      model_name="svm_baseline",
                                                      results_dict=svm_res)

            counter += 1
            
    return baseline_results 


def run_nonneural_return_results(languages=["en", "es"], tasks =["a","b"], aug_features={}, max_features=30000):

    nonneural_results = pd.DataFrame(columns=["task", "dataset", "dataset_lang",
                         "input_matrix", "aug_features", "model_cat", "model", "macro_avg_f1", "accuracy", "precision", "recall", "emr"])
    
    task_b_switch = False
    counter = 0
    
    for task in tasks:
        
        print("Task: ", task.upper(), "\n")  
        
        if task == "b":
            task_b_switch = True
        
        for language in languages:
            
            print("Language: {} \n".format("English" if language == "en" else "Spanish"))

            maximum_features = max_features
            
            #task_b = task_b_switch
            if language == "en":
                print("In English")
            else:
                print("In Spanish")
                
            fdir = "../data/preprocessed/"
            train_data_f = os.path.join(fdir, "dev/preproc_train_%s.tsv"%language)
            test_data_f = os.path.join(fdir, "reference_test/preproc_%s.tsv"%language)
            train_lb_df = pd.read_csv(train_data_f, sep='\t', header=0).iloc[:,2:5]
            test_lb_df = pd.read_csv(test_data_f, sep='\t', header=0).iloc[:,2:5]
            train_dat_df = pd.read_csv(train_data_f, sep='\t', header=0).iloc[:,1].values.tolist()
            test_dat_df = pd.read_csv(test_data_f, sep='\t', header=0).iloc[:,1].values.tolist()
            #15k in vocabulary


            # call functions
            ngram_lr_res = nonneural.run_ngram_lr(train_dat_df, test_dat_df, train_lb_df, test_lb_df, task_b_switch, maximum_features,
                                                  augment=aug_features)
            
                   
            nonneural_results.loc[counter, :] = format_results(task=task,
                                                              lang=language,
                                                              input_X="ngrams",
                                                              aug_features=[None],
                                                              model_category="non_neural",
                                                              model_name="ngram_logistic_reg",
                                                              results_dict=ngram_lr_res)

            counter += 1

            
    return nonneural_results 

baseline_results_df = run_baselines_return_results(languages=["en", "es"], tasks =["a","b"])

nonneural_results_df = run_nonneural_return_results(languages=["en","es"], tasks =["a","b"], max_features=30000)


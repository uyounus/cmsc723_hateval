# +
# coding: utf-8

#get_ipython().run_line_magic('matplotlib', 'inline')
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import networkx as nx
import pickle
import configparser
import os
from itertools import chain 
import pygraphviz
from networkx.drawing.nx_agraph import write_dot
import pydot
import pandas as pd
from collections import OrderedDict

def create_vid_lookup_table(config, dataset, vtypes):
    
    node_ids = OrderedDict()
    id_counter = 0
    
    for v in vtypes:
        pickle_file = os.path.join(config["paths"]["EDGE_LIST_PATH"], "{}_{}".format(v, dataset))
        
        with open("{}.pickle".format(pickle_file), "rb") as input_dict:
            rel_dict = pickle.load(input_dict)
        
        # The keys should always correspond to tweet_ids; they only need to be inserted once.
        for k in rel_dict.keys():
            kkey = "{}".format(k)
            if kkey not in node_ids.keys():
                node_ids["{}".format(k)] = {"vid": id_counter, "vtype": "tweet_id", "vtext": "{}".format(k)}
                id_counter +=1 
        
        # Some tweets contain multiple items for any given vertex type; we need to collect ALL items
        # The emojis are collected in a counter; since the format is different, need to handle differently
        
        if v == "emojis":
            fv = [x.keys() for x in rel_dict.values()]
            feature_vertices = list(set(chain.from_iterable(x for x in fv))) 

        else:
            fv = [x for x in rel_dict.values()]
            feature_vertices = list(set(chain.from_iterable(x.values() for x in fv)))   
        
        for f in feature_vertices:

            fkey = "{}".format(f)
            if fkey not in node_ids.keys():
                node_ids[fkey] = {"vid": id_counter, "vtype": v, "vtext": "{}".format(f)}
                id_counter +=1 

    # node_ids now contains a unique integer identifier for each vertex (e.g., for all vtypes)
    return node_ids

def create_vertex_lists(config, dataset, node_id_dict, vtypes, data_dir):
    
    # It is easier to preserve/color by vertex type in neo4j if we export each vtype df separately    
    vlist_df = pd.DataFrame.from_dict(node_id_dict, orient="index")
    vlist_df.to_csv(os.path.join(data_dir, "{}_vlist_full.csv".format(dataset)), index=False)
    
    for v in vtypes:
        vlist_filtered = vlist_df[vlist_df["vtype"] == v]
        vlist_filtered.to_csv(os.path.join(data_dir, "{}_vlist_{}.csv".format(dataset, v)), index=False)

    vlist_filtered_tweetids = vlist_df[vlist_df["vtype"] == "tweet_id"]
    vlist_filtered_tweetids.to_csv(os.path.join(data_dir, "{}_vlist_tweet_ids.csv".format(dataset)), index=False)
        
    return vlist_df

def create_emoji_lists(emojidf, node_id_dict):
    # a stupid function that I'll make less stupid if we have enough time; for now, does the trick
    
    tweet_emoji_lists_dict = OrderedDict()
    
    for i, row in emojidf.iterrows():
        tweet_emoji_lists_dict[str(int(row["index"]))] = []
    
    for i, row in emojidf.iterrows():
        
        for j, c in enumerate(emojidf.columns[1:]):
            if not np.isnan(row[c]):
                for k in range(0, int(row[c])):
                    tweet_emoji_lists_dict[str(int(row["index"]))].append(c)
        
    outdf = pd.DataFrame.from_dict(tweet_emoji_lists_dict, orient='index').reset_index()
    return outdf
    

def add_vertices_and_edges(config, dataset, node_id_dict, vtypes, graph):
    
    for v_type in vtypes:
    
        pickle_file = os.path.join(config["paths"]["EDGE_LIST_PATH"], "{}_{}".format(v_type, dataset))

        with open("{}.pickle".format(pickle_file), "rb") as input_dict:
            rel_dict = pickle.load(input_dict)
        
        tweet_vertices = rel_dict.keys()
        
        for v in tweet_vertices:
            
            vid = int(node_id_dict["{}".format(v)]["vid"])
            vtype = node_id_dict["{}".format(v)]["vtype"]
            vtext = node_id_dict["{}".format(v)]["vtext"]
            graph.add_node(vid, vtype=vtype, source=dataset, vtext =vtext)
            
        if v == "emojis":
            fv = [x.keys() for x in rel_dict.values()]
            feature_vertices = list(set(chain.from_iterable(x for x in fv))) 

        else:
            fv = [x for x in rel_dict.values()]
            feature_vertices = list(set(chain.from_iterable(x.values() for x in fv)))     

        for v in feature_vertices:
            vid = int(node_id_dict["{}".format(v)]["vid"])
            vtype = node_id_dict["{}".format(v)]["vtype"]
            vtext = node_id_dict["{}".format(v)]["vtext"]
            graph.add_node(vid, vtype=vtype, source=dataset, vtext = vtext)
            
        df = pd.DataFrame.from_dict(rel_dict,orient="index").reset_index()
                  
        if v_type == "emojis":
            df = create_emoji_lists(df, node_id_dict)

        else:
            df.index = df.index.astype(int).astype(str)
        
        for i, row in df.iterrows():

            src = row["index"]

            if v_type == "emojis":
                dsts = [x for x in row[1:] if x is not None]
            
            else:
                dsts = [x for x in row[1:] if x is not np.nan]

            src_id = node_id_dict["{}".format(int(src))]["vid"]
            
            for j,d in enumerate(dsts):
                
                dst_id = node_id_dict["{}".format(d)]["vid"]
                
                if graph.has_edge(src_id, dst_id):
                    graph[src_id][dst_id]["weight"] +=1
                else:
                    graph.add_edge(src_id, dst_id, weight=1, etype="CONTAINS")
                
                if j < (len(dsts) -1):
                    
                    next_dst_id = node_id_dict["{}".format(dsts[j+1])]["vid"]
                    
                    if graph.has_edge(dst_id, next_dst_id):
                        graph[dst_id][next_dst_id]["weight"] +=1
                    else:            
                        graph.add_edge(dst_id, next_dst_id, weight=1, etype="CO_OCCUR")
                else:
                    continue

    return graph


def write_edgelist_to_file(data_dir, filename, dataset, graph):
    
    v0_filename = os.path.join(data_dir, "{}_{}_v0.csv".format(dataset, filename))
    v1_filename = os.path.join(data_dir, "{}_{}_v1.csv".format(dataset, filename))
    
    nx.write_edgelist(graph, v0_filename, data=['weight','etype'])

    with open(v0_filename) as infile, open(v1_filename, 'w') as outfile:
        outfile.write("src_vid,dst_vid,weight,etype\n")
        for line in infile:
            fields = line.split()
            outfile.write('{}\n'.format(','.join(fields)))

    return

def bg_main(config_file="../english_config.ini", dataset="train_en"):
    
    config = configparser.ConfigParser()
    config.read(config_file)

    #vertex_types = ["mentions", "hashtags", "urls", "expletives", "racial_slurs", "emojis", "names"]
    # emojis were causing a bug in the final graph augmentation code, so commenting out for now
    
    vertex_types = ["mentions", "hashtags", "urls", "expletives", "racial_slurs", "names"]
    
    if "reference_test" in dataset: 
        dataset_name = "{}".format(config["paths"]["LANG"])
    else:
        dataset_name = dataset
    
    node_ids = create_vid_lookup_table(config, dataset_name, vertex_types)
    
    vdf = create_vertex_lists(config, dataset_name, node_ids, vertex_types, config["paths"]["EDGE_LIST_PATH"])
    
    G = nx.Graph()
    G = add_vertices_and_edges(config, dataset_name, node_ids, vertex_types, G)
    
    write_edgelist_to_file(config["paths"]["EDGE_LIST_PATH"], "edgelist", dataset, G)
    
    return G
    
    

if __name__=="__main__":

   G = bg_main(config_file="../english_config.ini", dataset="train_en")
# -



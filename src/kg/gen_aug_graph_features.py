
# coding: utf-8

import networkx as nx
import configparser
import os
import numpy as np
import src.kg.build_graph
import src.kg.build_graph # uncomment this when not running in Jupyter nb
from collections import OrderedDict
import pandas as pd
#get_ipython().run_line_magic('run', 'build_graph.py')

def build_graph(config_file, dataset_type):
    
    G = src.kg.build_graph.bg_main(config_file, dataset_type)
    return G

def compute_edge_weight_quartiles(g):
    
    edge_weights = [e['weight'] for e in [g.get_edge_data(src, dst)for (src, dst) in g.edges]]
    
    return np.quantile(edge_weights, [.25, .50, .75, 1.0]), np.mean(edge_weights)


def create_dir_for_graph_aug_files(root_data_dir):
    
    graph_dir = os.path.join(root_data_dir, "preprocessed_graph_aug")
    dev_graph_dir = os.path.join(graph_dir, "dev")
    reftest_graph_dir = os.path.join(graph_dir, "reference_test")
    
    for new_dir in [graph_dir, dev_graph_dir, reftest_graph_dir]:
        if not os.path.isdir(new_dir):
            os.mkdir(new_dir)  
    return 


def create_graph_node_data_dict(graph):
    node_info = OrderedDict()
    
    for node_id, data in graph.nodes.data():
        node_info[node_id] = data
        
    return node_info


def map_extracted_feature_to_neigbhors_in_graph(graph, node_info_dict, feature, edge_weight_cutoff=1):  
    
    neighbors = []
    
    for k,v in node_info_dict.items():
        
        try:
            if v['vtext'] == feature:
                neighbors.append(x for x in [graph.nodes[n]['vtext'] for n in graph.neighbors(k) if
                                             graph.get_edge_data(k,n)['weight'] >= edge_weight_cutoff or
                                            graph.get_edge_data(n,k)['weight'] >= edge_weight_cutoff])
        except KeyError:
            continue
            
    return neighbors


def get_contain_edges_for_tweet_id(graph, tweet_id, node_info_dict):
    neighbors = []
    
    for k,v in node_info_dict.items():

        try:
            if v['vtext'] == tweet_id:

                neighbors= [node for node in graph.neighbors(k)]

        except KeyError:
            continue
        
    return neighbors

def get_neighbors_of_neighbors(graph, node_info_dict, neighbors_of_tweet, edge_weight_cutoff,
                               feature_types=["hashtags", "mentions", "expletives", "racial_slurs", "names"],
                              max_words_to_return= 15):
    
    text_of_second_order_neighbors = []
    
    text_to_augment_tweet = pd.DataFrame(columns=["vid", "vtext", "eweight"])
    
    for n in neighbors_of_tweet:
        text_of_second_order_neighbors.append([x, graph.nodes()[x]] for x in [nn for nn in graph.neighbors(n)
                                                         if graph.get_edge_data(nn,n)['weight'] >= edge_weight_cutoff
                                                         or graph.get_edge_data(n, nn)['weight'] >= edge_weight_cutoff])
        
        for i, sublist in enumerate(text_of_second_order_neighbors):
            for x in sublist:
 
                vid = int(x[0])
    
                try:
                
                    if x[1]['vtype'] in feature_types:
                        text_to_augment_tweet.loc[i, :] = [int(x[0]), x[1]['vtext'],  max(graph.get_edge_data(vid, n)['weight'],
                                                                        graph.get_edge_data(n,vid)['weight'])]
                except KeyError:
                    continue 
                    
    text_to_augment_tweet_out = text_to_augment_tweet.sort_values(by=['eweight'], ascending=False).reset_index(drop=True)
        
    return text_to_augment_tweet_out.loc[0:min(text_to_augment_tweet_out.shape[0], max_words_to_return),:]


def augment_train_dataset(config_file, dataset_type="train", lang="en", 
                          feature_types=["hashtags", "mentions", "expletives", "racial_slurs", "names"],
                          max_words=10):
    
    config = configparser.ConfigParser()
    config.read(config_file)
    
    create_dir_for_graph_aug_files(config["paths"]["DATA_DIR"])
    
    data_subdir = "dev" if dataset_type in ["dev", "train"] else "reference_test"
    file_name = "preproc_{}_{}.tsv".format(dataset_type, lang) if dataset_type in ["dev", "train"] else "{}_{}.tsv".format(dataset_type, lang)
    
    preproc_data_file = os.path.join(config["paths"]["PP_DATA_DIR"], data_subdir, file_name)

    G = build_graph(config_file, "{}_{}".format(dataset_type, lang))
    edge_weight_quartiles, mean_ew =  compute_edge_weight_quartiles(G)
    edge_weight_cutoff = min(edge_weight_quartiles[1], mean_ew)
    
    node_info_dict = create_graph_node_data_dict(G)
    
    orig_df = pd.read_csv(preproc_data_file, sep="\t", header=0)

    aug_df = pd.DataFrame(columns = orig_df.columns)
    
    for i, row in orig_df.iterrows():

        tid = str(row["id"])
        tneighbors = get_contain_edges_for_tweet_id(G, tid, node_info_dict)
        
        new_text = ' '.join(word for word in row['text'].split())
        
        if len(tneighbors) > 0:
            
            aug_words = get_neighbors_of_neighbors(G, node_info_dict, tneighbors, edge_weight_cutoff, 
                        feature_types=feature_types,
                        max_words_to_return = max_words)
            
            new_text += ' '.join(aw for aw in aug_words["vtext"])
        
        aug_df.loc[i, :] = [row["id"], new_text, row["HS"], row["TR"], row["AG"]]
    
    aug_df_filename = "graph_aug_{}".format(file_name)
    aug_df_path = os.path.join(config["paths"]["DATA_DIR"], "preprocessed_graph_aug", data_subdir, aug_df_filename)
    aug_df.to_csv(aug_df_path, header=0, sep="\t", index=False)

    return orig_df, aug_df,G, node_info_dict

def augment_valid_or_test_dataset(config_file, G_train, dataset_type="dev", lang="en", 
                          feature_types=["hashtags", "mentions", "expletives", "racial_slurs", "names"],
                          max_words=10):
    
    config = configparser.ConfigParser()
    config.read(config_file)
    
    data_subdir = "dev" if dataset_type in ["dev", "train"] else "reference_test"
    file_name = "preproc_{}_{}.tsv".format(dataset_type, lang) if dataset_type in ["dev", "train"] else "preproc_{}.tsv".format(lang)
    
    preproc_data_file = os.path.join(config["paths"]["PP_DATA_DIR"], data_subdir, file_name)

    G_valid_or_test = build_graph(config_file, "{}_{}".format(dataset_type, lang))
    
    G = nx.compose(G_train, G_valid_or_test)
    
    edge_weight_quartiles, mean_ew =  compute_edge_weight_quartiles(G)
    edge_weight_cutoff = min(edge_weight_quartiles[1], mean_ew)
    
    node_info_dict = create_graph_node_data_dict(G)
    
    orig_df = pd.read_csv(preproc_data_file, sep="\t", header=0)
    
    aug_df = pd.DataFrame(columns = orig_df.columns)
    
    for i, row in orig_df.iterrows():

        tid = str(row["id"])
        tneighbors = get_contain_edges_for_tweet_id(G, tid, node_info_dict)
        
        new_text = ' '.join(word for word in row['text'].split())
        
        if len(tneighbors) > 0:
            
            aug_words = get_neighbors_of_neighbors(G, node_info_dict, tneighbors, edge_weight_cutoff, 
                        feature_types=feature_types,
                        max_words_to_return = max_words)
            
            new_text += ' '.join(aw for aw in aug_words["vtext"])
        
        aug_df.loc[i, :] = [row["id"], new_text, row["HS"], row["TR"], row["AG"]]
    
    aug_df_filename = "graph_aug_{}".format(file_name)
    aug_df_path = os.path.join(config["paths"]["DATA_DIR"], "preprocessed_graph_aug", data_subdir, aug_df_filename)
    aug_df.to_csv(aug_df_path, header=0, sep="\t", index=False)

    return orig_df, aug_df, G, node_info_dict

def main(config_file ="../english_config.ini"):
    
    config = configparser.ConfigParser()
    config.read(config_file)

    LANG = config["paths"]["LANG"]
    DATASET = "train_{}".format(LANG)

    create_dir_for_graph_aug_files(config["paths"]["DATA_DIR"])
    
    G_train = build_graph(config_file, DATASET)
    node_info_dict = create_graph_node_data_dict(G_train)
    
    print("[INFO] Augmenting training dataset...")
    train_orig_df, train_aug_df, G_train, train_node_dict = augment_train_dataset(config_file, dataset_type="train", lang=LANG)
    print("[INFO] Training set augmented!")
    
    print("Augmenting valid dataset...")
    valid_orig_df, valid_aug_df, G_valid, valid_node_dict = augment_valid_or_test_dataset(config_file, G_train, dataset_type="dev", lang=LANG, 
                          feature_types=["hashtags", "mentions", "expletives", "racial_slurs", "names"],
                          max_words=10)
    print("[INFO] Valid set augmented!")
    
    print("Augmenting test dataset...")
    test_orig_df, test_aug_df, G_test, test_node_dict = augment_valid_or_test_dataset(config_file, G_train, dataset_type="reference_test", lang=LANG, 
                          feature_types=["hashtags", "mentions", "expletives", "racial_slurs", "names"],
                          max_words=10)
    print("[INFO] Test augmented!")
    print("[INFO] DONE: All graph-augmented datasets for language: {} created.".format(LANG))
    
    return train_aug_df, valid_aug_df, test_aug_df, G_train, G_valid, G_test

if __name__=="__main__":
    
    train_aug_dfE, valid_aug_dfE, test_aug_dfE, G_trainE, G_validE, G_testE = main(config_file ="../english_config.ini")
    train_aug_dfS, valid_aug_dfS, test_aug_dfS, G_trainS, G_validS, G_testS = main(config_file ="../spanish_config.ini")


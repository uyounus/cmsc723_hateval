# %%

# coding: utf-8

# %%
import pandas as pd
import numpy as np
import os
from collections import OrderedDict, Counter
import re
import textacy
from textacy import preprocessing
import spacy
import configparser
import random
import emoji
from spellchecker import SpellChecker
import sys
import platform
import pickle
from sklearn.preprocessing import StandardScaler


# %%
#sentiment analysis; average embedding per tweet; number of plural nouns in a tweet (for target classification); number of insults in a tweet (for aggression classification); (2) part-of-speech and/or dependency relations; and (3) entity relations


# %%
#["mentions", "hashtags", "urls", "expletives", "racial_slurs", "emojis", "names"]
# all the dims are hardcoded; need to fix that so that it can handle the augmented feature matrix 

# PREPROC_DATA_DIR = "../../data/preprocessed"
# DATA_SUBDIR = "dev"
# DATA_TYPE = "train" # in [train, dev, reference_test]
# EDGE_LIST_DIR = "../../data/edge_lists"
# LANG = "en"


# %%


def gen_lexical_features(df, edge_list_dir:str, feature_type:str, data_type:str, lang:str):

    if data_type == "reference_test":
        feature_dict_path = os.path.join(edge_list_dir, "{}_{}".format(feature_type, lang))
    
    else:
        feature_dict_path = os.path.join(edge_list_dir, "{}_{}_{}".format(feature_type, 
                                                                        data_type, lang))
    
    with open("{}.pickle".format(feature_dict_path), "rb") as d:
            feature_dict = pickle.load(d)

    count_features = OrderedDict()
    bool_features = OrderedDict()

    for k,v in feature_dict.items():
        count_features[k] = len(v.keys())
        bool_features[k] = int(len(v.keys()) > 0)
        
    cfdf = pd.DataFrame.from_dict(count_features, orient='index').reset_index()
    cfdf.columns = ["id", "count_{}".format(feature_type)]
    
    bfdf = pd.DataFrame.from_dict(bool_features, orient='index').reset_index()
    bfdf.columns = ["id", "bool_{}".format(feature_type)]
    
    return cfdf, bfdf
                               

def gen_count_and_bool_features(preproc_dir, edge_list_dir, lang, dsubdir, dtype, dataset, features):
    
    orig_df = pd.read_csv(os.path.join(preproc_dir, dsubdir, dataset), sep="\t", header=None)
    orig_df = orig_df.loc[:, 0:4]
    
    orig_df.columns = ["id", "text", "HS", "TR", "AG"]
    feature_dfs = []
    
    
    for f in features:
        
        f_counts, f_bools = gen_lexical_features(orig_df, edge_list_dir, 
                                                   f, dtype, lang)
        feature_dfs.append(f_counts)
        feature_dfs.append(f_bools)
    
    aug_df = pd.DataFrame(orig_df.id, columns=["id"])
       
    for fdf in feature_dfs:
        aug_df = pd.merge(aug_df, fdf, on="id")

    #aug_df.columns = [x if x == "id" else x[:-2] for x in aug_df.columns]
    scaler = StandardScaler()
    aug_df.iloc[:,1:] = scaler.fit_transform(aug_df.iloc[:,1:])

    return aug_df


# %%
# features = ["mentions", "hashtags", "urls", "expletives", "racial_slurs", "emojis", "names"]

# augdf = gen_count_and_bool_features(PREPROC_DATA_DIR, EDGE_LIST_DIR, LANG, DATA_SUBDIR, DATA_TYPE, 
#                                     "preproc_train_en.tsv", features)


# %%
#augdf.head()

# %%

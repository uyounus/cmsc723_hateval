import pickle
from sklearn.metrics import accuracy_score, precision_score, recall_score, f1_score, confusion_matrix


def list2num(pred_ls, real_ls, task_b=False):
    """
    This function only used in computing exact match ratio (EMR) in task B
    input: predicted list, groundtruth list
    output: multiclass indicators
    e.g. for class label [1,0,1], the multiclass indicator is `101`
    if a list of labels is [[1,0,1],[0,0,1]], then the returned list is ['101', '001']
    """
    for i in range(len(pred_ls)):
        #pred_ls is of the same length of real_ls
        pred_y = [''.join([str(int(i0)) for i0 in item0]) for item0 in pred_ls]
        real_y = [''.join([str(int(i1)) for i1 in item1]) for item1 in real_ls]
    return pred_y, real_y

def task_a(path):
    try:
        with open(path,"rb") as fp:
            real_ls = pickle.load(fp)
            pred_ls = pickle.load(fp)
    except FileExistsError:
        print("file not exists")
        return
    acc = accuracy_score(real_ls,pred_ls)
    print("accuracy:")
    print(acc)
    prec = precision_score(real_ls,pred_ls)
    print("precision:")
    print(prec)
    recall = recall_score(real_ls,pred_ls)
    print("recall")
    print(recall)
    f1 = f1_score(real_ls,pred_ls)
    print("f1")
    print(f1)
    
    return {"macro_avg_f1":f1, "acc":acc, "pr":prec, "rs":recall}

# for evaluation during training
def _task_a(real_ls, pred_ls):
    acc = accuracy_score(real_ls,pred_ls)
    prec = precision_score(real_ls,pred_ls)
    recall = recall_score(real_ls,pred_ls)
    f1 = f1_score(real_ls,pred_ls)
    
    return {"macro_avg_f1":f1, "acc":acc, "pr":prec, "rs":recall}


def task_b(hs_path,tr_path,ag_path):
    try:
        with open(hs_path,"rb") as fp:
            real_hs_ls = pickle.load(fp)
            pred_hs_ls = pickle.load(fp)
        with open(tr_path,"rb") as fp:
            real_tr_ls = pickle.load(fp)
            pred_tr_ls = pickle.load(fp)
        with open(ag_path,"rb") as fp:
            real_ag_ls = pickle.load(fp)
            pred_ag_ls = pickle.load(fp)
    except FileExistsError:
        print("file not exists")
        return    
    real_ls = list(zip(real_hs_ls,real_tr_ls,real_ag_ls))
    pred_ls = list(zip(pred_hs_ls,pred_tr_ls,pred_ag_ls))
    real_ls,pred_ls = list2num(real_ls,pred_ls)
    f1_hs = f1_score(real_hs_ls,pred_hs_ls)
    f1_tr = f1_score(real_tr_ls,pred_tr_ls)
    f1_ag = f1_score(real_ag_ls,pred_ag_ls)
    avg_f1 = (f1_hs + f1_tr + f1_ag)/3
    emr = accuracy_score(real_ls,pred_ls)
    print("f1_hs")
    print(f1_hs)
    print("f1_tr")
    print(f1_tr)
    print("f1_ag")
    print(f1_ag)
    print("avf_f1")
    print(avg_f1)
    print("emr")
    print(emr)
    
    return {"macro_avg_f1":avg_f1, "f1_hs":f1_hs, "f1_tr":f1_tr, "f1_ag":f1_ag, "emr":emr}

# for evaluation during training
def _task_b(real_hs_ls, pred_hs_ls, real_tr_ls, pred_tr_ls, real_ag_ls, pred_ag_ls):
    real_ls = list(zip(real_hs_ls,real_tr_ls,real_ag_ls))
    pred_ls = list(zip(pred_hs_ls,pred_tr_ls,pred_ag_ls))
    real_ls,pred_ls = list2num(real_ls,pred_ls)
    f1_hs = f1_score(real_hs_ls,pred_hs_ls)
    f1_tr = f1_score(real_tr_ls,pred_tr_ls)
    f1_ag = f1_score(real_ag_ls,pred_ag_ls)
    avg_f1 = (f1_hs + f1_tr + f1_ag)/3
    emr = accuracy_score(real_ls,pred_ls)
   
    return {"macro_avg_f1":avg_f1, "f1_hs":f1_hs, "f1_tr":f1_tr, "f1_ag":f1_ag, "emr":emr}

if __name__ =="__main__":
    en_hs_path = "../../data/results/en_HS"
    en_tr_path = "../../data/results/en_TR"
    en_ag_path = "../../data/results/en_AG"
    es_hs_path = "../../data/results/es_HS"
    es_tr_path = "../../data/results/es_TR"
    es_ag_path = "../../data/results/es_AG"
    print("TASK A English")
    task_a(en_hs_path)
    print("TASK B English")
    task_b(en_hs_path,en_tr_path,en_ag_path)
    print("TASK A Spanish")
    task_a(es_hs_path)
    print("TASK B Spanish")
    task_b(es_hs_path,es_tr_path,es_ag_path)















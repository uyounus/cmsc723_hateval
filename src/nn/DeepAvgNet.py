import torch.nn as nn
import torch.nn.functional as F

class DeepAvgNet(nn.Module):
    def __init__(self, vocab_size, embed_dim, hid_dim, num_hid, num_class):
        super().__init__()
        #self.embedding = nn.EmbeddingBag(vocab_size, embed_dim, sparse=True)
        self.embedding = nn.EmbeddingBag(vocab_size, embed_dim)
        self.hid_list = nn.ModuleList()
        self.hid_list.append(nn.Linear(embed_dim, hid_dim))
        for i in range(num_hid-1):
            self.hid_list.append(nn.Linear(hid_dim, hid_dim))
        self.hid_list.append(nn.Linear(hid_dim, num_class))

        #activation functions
        self.activation = nn.ReLU()
        self.softmax = nn.Softmax(dim=1) #dim: compute softmax across which column
        
        self.init_weights()

    def init_weights(self):
        initrange = 0.5
        self.embedding.weight.data.uniform_(-initrange, initrange)
        #TODO load pre-trained word embeddings (e.g. word2vec)
        for fc in self.hid_list:
            fc.weight.data.uniform_(-initrange, initrange)
            fc.bias.data.zero_()

    def forward(self, text, offsets):
        x = self.embedding(text, offsets)
        for fc in self.hid_list[:-1]: #need to apply softmax instead of <activation> after the last hidden layer
            x = fc(x)
            x = self.activation(x)
        x = self.hid_list[-1](x)
        x = self.softmax(x)
        return x

tagsA=('HS')
tagsB=('HS' 'TR' 'AG')
tasks=('a' 'b')
#inputX=('base' 'count_bool' 'graph')
inputX=('base')

# for running code
for i in "${tasks[@]}"; do
  if [ "$i" = "a" ]; then
    for j in "${tagsA[@]}"; do
        for k in "${inputX[@]}"; do
            echo "Processing Task $i, label $j, input: $k"
            source activate cmsc723;
            #source activate coq_gym;
            python3 rnnKeras.py ../spanish_config.ini $j $i $k
            python3 rnnKeras.py ../english_config.ini $j $i $k
#             python3 rnnKeras.py ../spanish_config_ice.ini $j $i $k
#             python3 rnnKeras.py ../english_config_ice.ini $j $i $k&
        done
    done
  else
     # Task B
    for j in "${tagsB[@]}"; do
       for k in "${inputX[@]}"; do
            echo "Processing Task $i, label $j, input: $k"
            source activate cmsc723;
#             source activate coq_gym;
#             python3 rnnKeras.py ../spanish_config_ice.ini $j $i $k
#             python3 rnnKeras.py ../english_config_ice.ini $j $i $k&
            python3 rnnKeras.py ../spanish_config.ini $j $i $k
            python3 rnnKeras.py ../english_config.ini $j $i $k
        done
      done
   fi  
done
#wait
#python3 cal_metrics.py 



# # for running code
# for i in "${tags[@]}"; do
#   echo "Processing $i"
# 	# source activate cmsc723;
#     source activate coq_gym;
#     python3 rnnKeras.py ../spanish_config_ice.ini $i
#     python3 rnnKeras.py ../english_config_ice.ini $i &
# done
# wait
# python3 cal_metrics.py 

import pandas as pd
import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
from keras.preprocessing import text, sequence
from torch.utils.data import TensorDataset
from collections import OrderedDict
import configparser
import sys
import pickle
import fasttext
import os
import numpy as np
import datetime
from cal_metrics import list2num, task_a, task_b
from cal_metrics import _task_a, _task_b # for evaluation during training
# Modified from: https://www.analyticsvidhya.com/blog/2019/01/guide-pytorch-neural-networks-case-studies/

# Create all of the subdirectories we need to store model results and trained models 
def create_results_subdirs(data_dir, langs=["en", "es"], tasks=["a", "b"]):
       
    results_dir = os.path.join(data_dir, "neural_results")
    trained_models_dir = os.path.join(data_dir, "trained_models") 
    loss_dir = os.path.join(data_dir, "loss_files")
    
    for new_dir in [results_dir, trained_models_dir, loss_dir]:
        
        if not os.path.isdir(new_dir):
            os.mkdir(new_dir)   
        
    for lang in langs:
        
        res_lang_dir = os.path.join(results_dir, lang)
        models_lang_dir = os.path.join(trained_models_dir, lang)
        loss_lang_dir = os.path.join(loss_dir, lang)
        
        for new_dir in [res_lang_dir, models_lang_dir, loss_lang_dir]:
        
            if not os.path.isdir(new_dir):
                os.mkdir(new_dir)   
            
        for task in tasks:
            
            res_task_dir = os.path.join(res_lang_dir, task)
            models_task_dir = os.path.join(models_lang_dir, task)
            loss_task_dir =  os.path.join(loss_lang_dir, task)
            
            for new_dir in [res_task_dir, models_task_dir, loss_task_dir]:
        
                if not os.path.isdir(new_dir):
                    os.mkdir(new_dir)     
                
            if task == "a":
                batch_num = -1
                
            if task == "b":
                batches = []
                for (dirpath, dirnames, filenames) in os.walk(res_task_dir):
                    for d in dirnames:
                        try: 
                            batches.append(d.split("batch_")[1])
                        except IndexError:
                            continue
                    
                if len(batches) >0:
                    batch_num = int(batches[-1]) 
                    
                    if os.path.exists(os.path.join(res_task_dir, "batch_{}".format(batch_num), "{}_{}".format(lang, task))):
                        batch_num += 1
                else:
                    batch_num = 0
                
                res_task_batch_dir = os.path.join(res_task_dir, "batch_{}".format(batch_num))
                models_task_batch_dir = os.path.join(models_task_dir, "batch_{}".format(batch_num))
                loss_task_batch_dir =  os.path.join(loss_task_dir, "batch_{}".format(batch_num))
                
                for new_dir in [res_task_batch_dir, models_task_batch_dir, loss_task_batch_dir]:
        
                    if not os.path.isdir(new_dir):
                        os.mkdir(new_dir)      
                
    return results_dir, trained_models_dir, loss_dir, batch_num


def name_trained_model(trained_model_dir, hidden_size, lang, task, embeds_file, input_X, num_epochs, macro_avg_f1, target, batch_num):
    macro_avg_f1_round = np.round(macro_avg_f1, 3)
    model_name = "{}_{}_bi-lstm{}_{}_{}_{}_{}_{}.pickle".format(macro_avg_f1_round, target, hidden_size, lang, embeds_file, num_epochs,
                                                   input_X, datetime.datetime.now().strftime("%m_%d_%H%M"))
    
    if task == "a":
        model_path = os.path.join(trained_model_dir, lang, task, str(model_name))
        
    elif task == "b":
        model_path = os.path.join(trained_model_dir, lang, task, "batch_{}".format(batch_num), str(model_name))
        
    return model_path

def name_loss_file(loss_dir, hidden_size, lang, task, embeds_file, input_X, num_epochs, target, batch_num):
    loss_file_name = "{}_bi-lstm{}_{}_{}_{}_{}_{}.txt".format(target, hidden_size, lang, embeds_file, num_epochs,
                                                   input_X, datetime.datetime.now().strftime("%m_%d_%H%M"))

    if task == "a":
        file_path = os.path.join(loss_dir, lang, task, str(loss_file_name))
        
    elif task == "b":
        file_path = os.path.join(loss_dir, lang, task, "batch_{}".format(batch_num), str(loss_file_name))
        
    return file_path
            

class Model(nn.Module):
    def __init__(self,embedding_matrix,vocab_size,maxlen,embedding_len,batch_size,model_layers,hidden_size):
        super(Model, self).__init__()

        ## Embedding Layer, Add parameter
        # max_features (== max len?), embed_dim
        # 15089 is the vocab size of the English training dataset
        self.batch_size = batch_size
        self.maxlen = maxlen
        self.embedding_len = embedding_len
        self.embedding = nn.Embedding(vocab_size, embedding_len)
        et = torch.tensor(embedding_matrix, dtype=torch.float32)
        self.embedding.weight = nn.Parameter(et)
        self.embedding.weight.requires_grad = False

        # self.embedding_dropout = nn.Dropout2d(0.1)
        #self.num_layers = 2
        self.hidden_size  = hidden_size
        self.lstm = nn.LSTM(embedding_len, hidden_size, bidirectional=True, batch_first=True)
        # why 16 here
        #self.linear = nn.Linear(hidden_size, 16)
        self.linear = nn.Linear(hidden_size * 2, 16)
        self.out = nn.Linear(16, 1)
        self.relu = nn.ReLU()

        self.hidden = self.init_hidden()

        # self.lstm = nn.LSTM(200,512, 2)
        # self.fc = nn.Linear(512, 1)

    def init_hidden(self):
        return (torch.zeros(self.batch_size,self.maxlen, self.embedding_len), torch.zeros(self.batch_size,self.maxlen, self.embedding_len)) #(batch_size, seq_len, hid_dim)


    def forward(self, x):
        h_embedding = self.embedding(x)
        #print(h_embedding.size()) #torch.Size([128=batch_size, 150=seq_len, 200=emb_dim])
        h_lstm, _ = self.lstm(h_embedding)
        linear = self.relu(self.linear(h_lstm[:, -1, :])) # get last hidden state
        out = self.out(linear)
        out = torch.sigmoid(out) # for BCELoss need this

        return out


# Embedding construction
# load embeddings from path and construct embedding_matrix
def load_embeddings(embedding_file_path,vocab_size,word_index):
 
    if embedding_file_path.endswith("txt"):
        embeddings_index = {}
        embedding_len = 0
        for i, line in enumerate(open(embedding_file_path)):
            val = line.split()
            embeddings_index[val[0]] = np.asarray(val[1:], dtype='float32')
            embedding_len = len(embeddings_index[val[0]])

        # keep 200 here for now, need to change it later
        # assert(embedding_len == 200)
        embedding_matrix = np.zeros((vocab_size + 1, embedding_len))
        for word, i in word_index.items():
            embedding_vector = embeddings_index.get(word)
            
            if embedding_vector is not None:
                embedding_matrix[i] = embedding_vector
        return embedding_matrix,embedding_len
    
    elif embedding_file_path.endswith("bin"):
        model = fasttext.load_model(embedding_file_path)
        embedding_len = model.get_dimension()
        embedding_matrix = np.zeros((vocab_size + 1, embedding_len))
        
        for word, i in word_index.items():
            embedding_vector = model.get_word_vector(word)
            embedding_len = len(embedding_vector)
            
            if embedding_vector is not None:
                embedding_matrix[i] = embedding_vector
                
        return embedding_matrix,embedding_len


def data_preprocess(settings,label,embedding_file_path, device):

    #load config
    train_data_path = settings["bilstm"]["train_data_path"]
    valid_data_path = settings["bilstm"]["valid_data_path"]
    test_data_path = settings["bilstm"]["test_data_path"]
    num_words = int(settings["bilstm"]["num_words"])
    maxlen = int(settings["bilstm"]["maxlen"])
    epoch_num = int(settings["bilstm"]["epoch_num"])
    batch_size = int(settings["bilstm"]["batch_size"])
    model_layers = int(settings["bilstm"]["model_layers"])
    hidden_size = int(settings["bilstm"]["hidden_size"])
    train = pd.read_csv(train_data_path, sep="\t", header= 0)
    valid = pd.read_csv(valid_data_path, sep="\t", header= 0)
    test = pd.read_csv(test_data_path , sep="\t", header= 0)

    x_train = train["text"].values
    y_train = train[label].values
    x_valid = valid["text"].values
    y_valid = valid[label].values
    x_test = test["text"].values
    y_test = test[label].values
    # create tokens
    tokenizer = text.Tokenizer(num_words = num_words) 
    tokenizer.fit_on_texts(x_train)
    word_index = tokenizer.word_index
    num_words = len(word_index)
    
    embedding_matrix,embedding_len = load_embeddings(embedding_file_path,num_words,word_index)
    model = Model(embedding_matrix,num_words,maxlen,embedding_len,batch_size,
    model_layers,hidden_size)

    if device == torch.device("cuda"):
        print("Using cuda; setting model to model.cuda()")
        model = model.cuda()
        torch.set_default_tensor_type('torch.cuda.FloatTensor')

    # assert(num_words<num_words) #make sure the assumption above is true
    x_train = tokenizer.texts_to_sequences(x_train)
    x_train = sequence.pad_sequences(x_train, maxlen = maxlen)
    x_valid = tokenizer.texts_to_sequences(x_valid)
    x_valid = sequence.pad_sequences(x_valid, maxlen = maxlen)
    x_test = tokenizer.texts_to_sequences(x_test)
    x_test = sequence.pad_sequences(x_test, maxlen = maxlen)

    # return (x_train,y_train,x_valid,y_valid)
    x_tr = torch.tensor(x_train, dtype=torch.long)
    y_tr = torch.tensor(y_train, dtype=torch.float32)
    train = TensorDataset(x_tr, y_tr)
    trainloader = torch.utils.data.DataLoader(train, batch_size=batch_size, shuffle=True)
    #
    x_val = torch.tensor(x_valid, dtype=torch.long)
    y_val = torch.tensor(y_valid, dtype=torch.float32)
    valid = TensorDataset(x_val, y_val)
    validloader = torch.utils.data.DataLoader(valid, batch_size=batch_size)

    x_test = torch.tensor(x_test, dtype=torch.long)
    y_test = torch.tensor(y_test, dtype=torch.float32)
    test = TensorDataset(x_test, y_test)
    testloader = torch.utils.data.DataLoader(test, batch_size=batch_size)

    return trainloader,validloader,testloader,epoch_num,model

def run_model(trainloader,validloader,testloader, epoch_num,model, device, clip, loss_dir, batch_num, task, lang, embeds_file, input_X, target):

    loss_function = nn.BCELoss() # need sigmoid() before output
    optimizer = torch.optim.Adam(model.parameters())

    train_loss = OrderedDict()
    valid_loss = OrderedDict()
    test_loss = OrderedDict()
    
    loss_file_path = name_loss_file(loss_dir, model.hidden_size, lang, task, embeds_file, input_X, epoch_num, target, batch_num)
    
    f = open(loss_file_path, "w+")
    #headers = ["epoch", "avg_train_loss", "avg_valid_loss", "avg_test_loss", "\n"]
    headers = ["epoch", "avg_train_loss", "avg_valid_loss", "avg_test_loss", "val_f1", "val_acc", "val_pr", "val_rs", "test_f1", "test_acc", "test_pr", "test_rs", "\n"]
    f.write(','.join(x for x in headers))
    

    ## run for 10 Epochs
    for epoch in range(1, epoch_num+1):

        train_loss[epoch] = []
        valid_loss[epoch] = []
        test_loss[epoch]  = []
         #train_loss, valid_loss = [], []

        ## training part
        model.train()
        for data, target in trainloader:
            optimizer.zero_grad()

            output = model(data)
            output = output.squeeze()
            target = target.squeeze()
            #print("[train] model output: ", output.detach().numpy()[:20])
            #print("[train] target: ", target)
            #loss = loss_function(output, target.view(-1, 1))
            loss = loss_function(output, target)
            loss.backward()
            torch.nn.utils.clip_grad_norm_(model.parameters(), clip)
            optimizer.step()
            train_loss[epoch].append(loss.item())

        ## evaluation part
        model.eval()
        with torch.no_grad():
            ### validation set ###
            all_valid_reals = [] #FIXME very bad for doing this repeatedly
            all_valid_preds = []
            for data, target in validloader:
                valid_output = model(data)
                valid_output = valid_output.squeeze()
                
                target = target.squeeze()
                #loss = loss_function(output, target.view(-1, 1))
                loss = loss_function(valid_output, target)
                valid_loss[epoch].append(loss.item())

                #_, preds_tensor = torch.max(output, 1)
                #preds = np.squeeze(preds_tensor.numpy())

                #preds = np.round(output.numpy())
                valid_preds = torch.round(valid_output) #can't use numpy with gpu without going to cpu first
                #print("data: ", data, "target: ", target, " model output: ", output.detach().numpy(), "model preds: ", preds)
                #print("model output: ", output.cpu().numpy()[:20],"model preds: ", preds[:20])
                all_valid_reals.extend(target.tolist())
                all_valid_preds.extend(valid_preds.tolist())
                            
            ### test set ###            
            all_test_reals = [] #FIXME very bad for doing this repeatedly
            all_test_preds = []
            for data, target in testloader:
                test_output = model(data)
                test_output = test_output.squeeze()
                
                target = target.squeeze()
                #loss = loss_function(output, target.view(-1, 1))
                loss = loss_function(test_output, target)
                test_loss[epoch].append(loss.item())

                #_, preds_tensor = torch.max(output, 1)
                #preds = np.squeeze(preds_tensor.numpy())

                #preds = np.round(output.numpy())
                test_preds = torch.round(test_output) #can't use numpy with gpu without going to cpu first
                #print("data: ", data, "target: ", target, " model output: ", output.detach().numpy(), "model preds: ", preds)
                #print("model output: ", output.cpu().numpy()[:20],"model preds: ", preds[:20]) 
                all_test_reals.extend(target.tolist())
                all_test_preds.extend(test_preds.tolist())


        print("EPOCH ", epoch, "TRAINING LOSS: ", np.mean(train_loss[epoch]), "\tVALID LOSS: ", np.mean(valid_loss[epoch]), "\tTEST LOSS: ", np.mean(test_loss[epoch]))
        if task == 'a':
            val_metrics = _task_a(all_valid_reals, all_valid_preds)
            test_metrics = _task_a(all_test_reals, all_test_preds)
            #print(val_metrics) # to be formatted
        elif task == 'b':
            #ALSO use task A metric functions, only need fq
            val_metrics = _task_a(all_valid_reals, all_valid_preds)
            test_metrics = _task_a(all_test_reals, all_test_preds)

        
        vals = [str(epoch), str(np.mean(train_loss[epoch])), np.mean(valid_loss[epoch]), np.mean(test_loss[epoch]), val_metrics["macro_avg_f1"], val_metrics["acc"], val_metrics["pr"], val_metrics["rs"], test_metrics["macro_avg_f1"], test_metrics["acc"], test_metrics["pr"], test_metrics["rs"], "\n"]
        f.write(','.join(str(x) for x in vals))
        
    f.close()
    return model


def run_all():
    # For REPRODUCIBILITY
    # https://pytorch.org/docs/stable/notes/randomness.html
    np.random.seed(123)
    torch.manual_seed(123)
    torch.cuda.manual_seed(123)
    torch.backends.cudnn.deterministic = True
    config = configparser.ConfigParser()
    #config.read("../english_config_ice.ini")
    config.read(sys.argv[1])
    label = sys.argv[2]
    task = sys.argv[3]
    input_X = sys.argv[4]
    lang = config["paths"]["LANG"]
    embedding_file_path = config["bilstm"]["embedding_file_path"]
    embeds_name = config["bilstm"]["embeds_name"]
    data_path = config["bilstm"]["data_path"]
    clip = config["bilstm"]["CLIP"]
    res_dir, trained_model_dir, loss_dir, batch_num = create_results_subdirs(data_path, langs=[lang], tasks=[task])
    
#     embedding_file_path = None
#     if lang =='en':
#         #embedding_file_path = "../../data/glove.twitter.27B/glove.twitter.27B.200d.txt"
#         embedding_file_path = "./vectors/glove.twitter.27B/glove.twitter.27B.200d.txt" #on ichehammer
#     elif lang == 'es':
#         #embedding_file_path = "../../data/wiki.es/wiki.es.bin"
#         #embedding_file_path = "./vectors/data/wiki.es/wiki.es.bin"
#         embedding_file_path = "./vectors/cc.es.300.bin" #on ichehammer
#     else:
#         # unsupported language
#         assert(0)

    # path to store resulsts is hardcoded
    #results_path = "../../data/results/"+str(lang)+"_"+str(label)  # needs to go to config
    
    #results_path = os.path.join(res_dir, lang, task, "{}_{}.pickle".format(lang, label))
    
    
    
    #rnn_model_path = "../../data/trained_models" #TODO: needs to go to config
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    trainloader,validloader,testloader,epoch_num,model = data_preprocess(config,label,embedding_file_path, device)
    run_model(trainloader,validloader, testloader, epoch_num,model, device, clip, loss_dir, batch_num, task, lang, embeds_name, input_X, label)
    write_results(model,testloader,res_dir, trained_model_dir, lang, task,label, embeds_name, input_X, epoch_num, batch_num)

def write_results(model,testloader,res_dir, trained_model_dir, language, task,label, embeds_name, inX, num_epochs, batch_num):
    target_array = []
    pred_array = []
    with torch.no_grad():
        for data, target in testloader:
            output = model(data)
            output = output.squeeze()
            target = target.squeeze()
            target_array.extend(target.cpu().numpy().tolist())
            #preds = np.round(output.cpu().numpy())
            preds = torch.round(output)
            pred_array.extend(preds.tolist())
            
    if task == "a":
        result_path =  os.path.join(res_dir, language, task, "{}_{}.pickle".format(language, label))
        
    elif task == "b":
        result_path = os.path.join(res_dir, language, task,
                      "batch_{}".format(batch_num), "{}_{}.pickle".format(language, label))
    
    
    with open(result_path,'wb+') as fp:
        pickle.dump(target_array,fp)
        pickle.dump(pred_array,fp)
        
    
    if task == "a":
        print("task a")
        res = task_a(result_path)
        print(res.items())
        macro_avg_f1 = res["macro_avg_f1"]
        print(macro_avg_f1)
        
    elif task == "b":
        if label == "AG":
            
            HS_path = os.path.join(res_dir, language, task,
                      "batch_{}".format(batch_num), "{}_HS.pickle".format(language))
            TR_path = os.path.join(res_dir, language, task,
                      "batch_{}".format(batch_num), "{}_TR.pickle".format(language))
            AG_path = os.path.join(res_dir, language, task,
                      "batch_{}".format(batch_num), "{}_AG.pickle".format(language))
            #hs_path,tr_path,ag_path
            res = task_b(HS_path, TR_path, AG_path)
            macro_avg_f1 = res["macro_avg_f1"]
            #print(macro_avg_f1) #confusing!!
            
        else:
            # we haven't done all 3 labels yet so can't compute macro avg. f1; return a dummy
            macro_avg_f1 = 0.0000000000
        
        
        #b_batch_dir = os.path.join(trained_model_dir, language, task, "batch_{}".format(batch_num))
        
    trained_model_path = name_trained_model(trained_model_dir, model.hidden_size, language, task, embeds_name, inX, num_epochs, macro_avg_f1, label, batch_num)

    print("Task: ", task, " lang: ", language, "trained model path", trained_model_path)
    
        
        # B results need their own subdirectory! 
        # TODO: change by hooking up to calc metrics script 
        #macro_avg_f1 = 0.73489534593

#     with open("{}/{}_rnn".format(rnn_model_path), 'wb+') as mp:
#         pickle.dump(model, mp)

    with open(trained_model_path, 'wb+') as handle:
        torch.save(model, handle)
       # pickle.dump(model, handle, protocol=pickle.HIGHEST_PROTOCOL)


if __name__=="__main__":
    run_all()

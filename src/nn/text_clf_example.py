#source: https://pytorch.org/tutorials/beginner/text_sentiment_ngrams_tutorial.html

import torch
import torchtext
from torchtext.datasets import text_classification
from torch.utils.data import DataLoader
from torch.utils.data.dataset import random_split
from DeepAvgNet import *
import os
import time

def generate_batch(batch):
    label = torch.tensor([entry[0] for entry in batch])
    text = [entry[1] for entry in batch]
    offsets = [0] + [len(entry) for entry in text]
    offsets = torch.tensor(offsets[:-1]).cumsum(dim=0)
    text = torch.cat(text)
    return text, offsets, label

def train_func(sub_train_, model, device, optimizer, criterion, batch_size_=32):
    # Train the model
    train_loss = 0
    train_acc = 0
    data = DataLoader(sub_train_, batch_size=batch_size_, shuffle=True, collate_fn=generate_batch)
    for i, (text, offsets, cls) in enumerate(data):
        optimizer.zero_grad()
        text, offsets, cls = text.to(device), offsets.to(device), cls.to(device)
        output = model(text, offsets)
        loss = criterion(output, cls)
        train_loss += loss.item()
        loss.backward()
        optimizer.step()
        train_acc += (output.argmax(1) == cls).sum().item()

    # Adjust the learning rate
    #scheduler.step() # this is for SGD

    return train_loss / len(sub_train_), train_acc / len(sub_train_)

def test(data_, model, device, criterion, batch_size_=32):
    loss = 0
    acc = 0
    data = DataLoader(data_, batch_size=batch_size_, collate_fn=generate_batch)
    for text, offsets, cls in data:
        text, offsets, cls = text.to(device), offsets.to(device), cls.to(device)
        with torch.no_grad():
            output = model(text, offsets)
            loss = criterion(output, cls)
            loss += loss.item()
            acc += (output.argmax(1) == cls).sum().item()

    return loss / len(data_), acc / len(data_)

### load data ###
tmp_path = os.path.join('./.data')
if not os.path.isdir(tmp_path):
    os.mkdir(tmp_path)

NGRAMS = 2
train_dataset, test_dataset = text_classification.DATASETS['AG_NEWS'](root='./.data', ngrams=NGRAMS, vocab=None)

### build model ###
VOCAB_SIZE = len(train_dataset.get_vocab())
EMBED_DIM = 300 # embedding size
HID_DIM = 512 # hidden layer size
NUM_HID = 2 # how many hidden layers
NUN_CLASS = len(train_dataset.get_labels())
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
model = DeepAvgNet(VOCAB_SIZE, EMBED_DIM, HID_DIM, NUM_HID, NUN_CLASS).to(device)

### training ###
min_valid_loss = float('inf')

criterion = torch.nn.CrossEntropyLoss().to(device)
#optimizer = torch.optim.SGD(model.parameters(), lr=4.0)
#scheduler = torch.optim.lr_scheduler.StepLR(optimizer, 1, gamma=0.9)
learning_rate = 1e-4
optimizer = torch.optim.Adam(model.parameters(), lr=learning_rate)
#learning_rate = 1e-2
#optimizer = torch.optim.RMSprop(model.parameters(), lr=learning_rate)
train_len = int(len(train_dataset) * 0.95)
sub_train_, sub_valid_ = random_split(train_dataset, [train_len, len(train_dataset) - train_len])

N_EPOCHS = 20
for epoch in range(N_EPOCHS):

    start_time = time.time()
    train_loss, train_acc = train_func(sub_train_, model, device, optimizer, criterion)
    valid_loss, valid_acc = test(sub_valid_, model, device, criterion)

    secs = int(time.time() - start_time)
    mins = secs / 60
    secs = secs % 60

    print('Epoch: %d' %(epoch + 1), " | time in %d minutes, %d seconds" %(mins, secs))
    print(f'\tLoss: {train_loss:.4f}(train)\t|\tAcc: {train_acc * 100:.1f}%(train)')
    print(f'\tLoss: {valid_loss:.4f}(valid)\t|\tAcc: {valid_acc * 100:.1f}%(valid)')


# from https://stanford.edu/~shervine/blog/pytorch-how-to-generate-data-parallel#dataset
# but may be better to stick with https://pytorch.org/text/_modules/torchtext/datasets/text_classification.html


import torch
from torch.utils import data

class Dataset(data.Dataset):
    def __init__(self, list_IDs, labels):
        self.labels = labels
        self.list_IDs = list_IDs

    def __len__(self):
        return len(self.list_IDs)

    def __getitem__(self, index):
        ID = self.list_IDs[index]

        # Load data and get label
        X = torch.load('home/jieli3000/hateval/data/preprocessed/reference_test/preproc_en.tsv')
        y = self.labels[ID]

        return X, y


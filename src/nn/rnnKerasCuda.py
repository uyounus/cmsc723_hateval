import pandas as pd
import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
from keras.preprocessing import text, sequence
from torch.utils.data import TensorDataset
from collections import OrderedDict

# https://www.analyticsvidhya.com/blog/2019/01/guide-pytorch-neural-networks-case-studies/

device = 'cuda' if torch.cuda.is_available() else 'cpu'

class Model(nn.Module):
    def __init__(self, device):
        super(Model, self).__init__()

        ## Embedding Layer, Add parameter
        # max_features (== max len?), embed_dim
        # 15089 is the vocab size of the English training dataset
        self.embedding = nn.Embedding(15089, 200)
        et = torch.tensor(embedding_matrix, dtype=torch.float32, device=device)
        self.embedding.weight = nn.Parameter(et)
        self.embedding.weight.requires_grad = False

        self.embedding_dropout = nn.Dropout2d(0.1)
        self.num_layers = 2
        self.hidden_size  = 512
        self.lstm = nn.LSTM(200, 512, batch_first=True)
        self.linear = nn.Linear(512, 16)
        self.out = nn.Linear(16, 1)
        self.relu = nn.ReLU()

        self.hidden = self.init_hidden()
        self.device = device
        # self.lstm = nn.LSTM(200,512, 2)
        # self.fc = nn.Linear(512, 1)

    # def forward(self, x):
    #     # Set initial states
    #
    #     # this set up is from:
    #
    #     x = self.embedding(x)
    #     h0 = torch.zeros(self.num_layers, x.size(1), self.hidden_size)  # 2 for bidirection
    #     c0 = torch.zeros(self.num_layers, x.size(1), self.hidden_size)
    #     #print(x.shape)
    #     # Forward propagate LSTM
    #     out, _ = self.lstm(x, (h0, c0))  # out: tensor of shape (batch_size, seq_length, hidden_size*2)
    #     #print(out.shape)
    #     # Decode the hidden state of the last time step
    #     out = self.fc(out[:, -1, :])
    #     return out

    def init_hidden(self):
        return (torch.zeros(128, 150, 512, device = device), torch.zeros(128, 150, 512, device = device)) #(batch_size, seq_len, hid_dim)


    def forward(self, x):
        h_embedding = self.embedding(x)
        #print(h_embedding.size()) #torch.Size([128=batch_size, 150=seq_len, 200=emb_dim])
        h_lstm, _ = self.lstm(h_embedding)
        #h_lstm, self.hidden = self.lstm(h_embedding, self.hidden)
        #print(h_lstm.size()) #torch.Size([128, 150, 512=hid_dim])
        #max_pool, _ = torch.max(h_lstm, 1)
        #linear = self.relu(self.linear(max_pool))
        linear = self.relu(self.linear(h_lstm[:, -1, :])) # get last hidden state
        out = self.out(linear)
        out = F.sigmoid(out) # for BCELoss need this

        return out

# TODO: sorry this is awful and hard-coded! will refactor ASAP.
train_data_path = "../../data/preprocessed/dev/preproc_train_en.tsv"
train = pd.read_csv(train_data_path, sep="\t", header= 0)
valid_data_path = "../../data/preprocessed/dev/preproc_dev_en.tsv"
valid = pd.read_csv(valid_data_path, sep="\t", header= 0)

x_train = train["text"].values
y_train = train['HS'].values

x_valid = valid["text"].values
y_valid = valid['HS'].values

np.random.seed(123)
torch.manual_seed(123)
torch.cuda.manual_seed(123)
torch.backends.cudnn.deterministic = True
#
## create tokens
tokenizer = text.Tokenizer(num_words = 20000)
tokenizer.fit_on_texts(x_train)
word_index = tokenizer.word_index

print(len(word_index))
#
## convert texts to padded sequences
x_train = tokenizer.texts_to_sequences(x_train)
x_train = sequence.pad_sequences(x_train, maxlen = 150)

x_valid = tokenizer.texts_to_sequences(x_valid)
x_valid = sequence.pad_sequences(x_valid, maxlen = 150)
#
EMBEDDING_FILE = './glove.twitter.27B/glove.twitter.27B.200d.txt'

# EMBEDDING_FILE = '.vector_cache/wiki.en.vec'
#
embeddings_index = {}
for i, line in enumerate(open(EMBEDDING_FILE)):
    val = line.split()
    embeddings_index[val[0]] = np.asarray(val[1:], dtype='float32')

#
embedding_matrix = np.zeros((len(word_index) + 1, 200))
for word, i in word_index.items():
    embedding_vector = embeddings_index.get(word)
    if embedding_vector is not None:
        embedding_matrix[i] = embedding_vector
#
model = Model(device)
#
# ## create iterator objects for train and valid datasets
x_tr = torch.tensor(x_train, dtype=torch.long, device=device)
y_tr = torch.tensor(y_train, dtype=torch.float32, device=device)
train = TensorDataset(x_tr, y_tr)
trainloader = torch.utils.data.DataLoader(train, batch_size=128, shuffle=True)
#
x_val = torch.tensor(x_valid, dtype=torch.long, device=device)
y_val = torch.tensor(y_valid, dtype=torch.float32, device=device)
valid = TensorDataset(x_val, y_val)
validloader = torch.utils.data.DataLoader(valid, batch_size=128)
#
#loss_function = nn.BCEWithLogitsLoss(reduction='mean')
loss_function = nn.BCELoss() # need sigmoid() before output
optimizer = torch.optim.Adam(model.parameters())

train_loss = OrderedDict()
valid_loss = OrderedDict()

CLIP = 1.0

## run for 10 Epochs
for epoch in range(1, 10+1):

    train_loss[epoch] = []
    valid_loss[epoch] = []
    #train_loss, valid_loss = [], []

    ## training part
    model.train()
    for data, target in trainloader:
        optimizer.zero_grad()

        output = model(data)
        output = output.squeeze()

        target = target.squeeze()
        #print("[train] model output: ", output.detach().numpy()[:20])
        #print("[train] target: ", target)
        #loss = loss_function(output, target.view(-1, 1))
        loss = loss_function(output, target)
        loss.backward()
        torch.nn.utils.clip_grad_norm_(model.parameters(), CLIP)
        optimizer.step()
        train_loss[epoch].append(loss.item())

    ## evaluation part
    model.eval()
    with torch.no_grad():
        for data, target in validloader:
        #for data, target in trainloader:
            #print(data.size())
            output = model(data)
            output = output.squeeze()
            
            target = target.squeeze()
            #loss = loss_function(output, target.view(-1, 1))
            loss = loss_function(output, target)
            valid_loss[epoch].append(loss.item())

            #_, preds_tensor = torch.max(output, 1)
            #preds = np.squeeze(preds_tensor.numpy())
            preds = np.round(output.numpy())
            #print("data: ", data, "target: ", target, " model output: ", output.detach().numpy(), "model preds: ", preds)
            print("model output: ", output.numpy()[:20],"model preds: ", preds[:20])

    print("EPOCH ", epoch, "TRAINING LOSS: ", sum(train_loss[epoch]))
    print("EPOCH ", epoch, "VALID LOSS: ", sum(valid_loss[epoch]))

"""
dataiter = iter(validloader)
data, labels = dataiter.next()
output = model(data)
_, preds_tensor = torch.max(output, 1)
preds = np.squeeze(preds_tensor.numpy())

print(labels)
print(preds)

print(train_loss)
print(valid_loss)
"""

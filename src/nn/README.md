# usage
`python rnnKeras.py ../*_confih.ini`

The embeddings files are under data now, eg, ../../data/glove.twitter.27B/glove.twitter.27B.200d.txt.


# Example
```
(cmsc723) :~/hateval/src/nn$ python text_clf_example.py
120000lines [00:07, 16781.03lines/s]
120000lines [00:13, 8666.47lines/s]
7600lines [00:00, 9652.76lines/s]
Epoch: 1  | time in 3 minutes, 0 seconds
        Loss: 0.0327(train)     |       Acc: 68.8%(train)
        Loss: 0.0003(valid)     |       Acc: 82.9%(valid)
Epoch: 2  | time in 3 minutes, 0 seconds
        Loss: 0.0268(train)     |       Acc: 88.7%(train)
        Loss: 0.0003(valid)     |       Acc: 86.7%(valid)
Epoch: 3  | time in 3 minutes, 0 seconds
        Loss: 0.0254(train)     |       Acc: 93.1%(train)
        Loss: 0.0003(valid)     |       Acc: 87.9%(valid)
Epoch: 4  | time in 3 minutes, 0 seconds
        Loss: 0.0248(train)     |       Acc: 95.1%(train)
        Loss: 0.0003(valid)     |       Acc: 87.7%(valid)
Epoch: 5  | time in 3 minutes, 0 seconds
        Loss: 0.0244(train)     |       Acc: 96.2%(train)
        Loss: 0.0003(valid)     |       Acc: 87.8%(valid)
Epoch: 6  | time in 3 minutes, 0 seconds
        Loss: 0.0242(train)     |       Acc: 96.9%(train)
        Loss: 0.0003(valid)     |       Acc: 87.8%(valid)
Epoch: 7  | time in 2 minutes, 54 seconds
        Loss: 0.0240(train)     |       Acc: 97.5%(train)
        Loss: 0.0003(valid)     |       Acc: 87.7%(valid)
Epoch: 8  | time in 2 minutes, 59 seconds
        Loss: 0.0239(train)     |       Acc: 97.8%(train)
        Loss: 0.0003(valid)     |       Acc: 88.0%(valid)
Epoch: 9  | time in 3 minutes, 0 seconds
        Loss: 0.0238(train)     |       Acc: 98.1%(train)
        Loss: 0.0003(valid)     |       Acc: 87.9%(valid)
Epoch: 10  | time in 3 minutes, 0 seconds
        Loss: 0.0238(train)     |       Acc: 98.3%(train)
        Loss: 0.0003(valid)     |       Acc: 88.2%(valid)
...
```
* Not verified with CPU

# Notes
* Environment Setup https://paper.dropbox.com/doc/CMSC723-Project-Setup--AocyCYypfYcdpMsW3nVdAeXHAQ-y6asd9qNsC3SOsWR2AKKY
* PyTorch Demo https://paper.dropbox.com/doc/CMSC723-Project-NN--Aoc8R7VvedMQoofE2WTUGkVSAg-nfIcie9uz7tvPgQVq6pAq
# TODO
* Replace the example dataset with our csv files

===
```
(cmsc723) ytshiue@nlg-wks-g:~/hateval/src/nn$ python rnnKeras.py
...
15089
EPOCH  1 TRAINING LOSS:  77.16063332557678
EPOCH  1 VALID LOSS:  8.972442746162415
EPOCH  2 TRAINING LOSS:  54.68263804912567
EPOCH  2 VALID LOSS:  5.68880420923233
EPOCH  3 TRAINING LOSS:  50.464518308639526
EPOCH  3 VALID LOSS:  5.659584879875183
EPOCH  4 TRAINING LOSS:  50.22016167640686
EPOCH  4 VALID LOSS:  5.636062026023865
EPOCH  5 TRAINING LOSS:  50.016580045223236
EPOCH  5 VALID LOSS:  5.615990519523621
EPOCH  6 TRAINING LOSS:  49.84053146839142
EPOCH  6 VALID LOSS:  5.598506391048431
EPOCH  7 TRAINING LOSS:  49.685963809490204
EPOCH  7 VALID LOSS:  5.583124458789825
EPOCH  8 TRAINING LOSS:  49.54919612407684
EPOCH  8 VALID LOSS:  5.569519877433777
EPOCH  9 TRAINING LOSS:  49.42762917280197
EPOCH  9 VALID LOSS:  5.557447969913483
...
```
